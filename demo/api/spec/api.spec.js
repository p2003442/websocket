import { decreaseTTl, updateZRR, updateTTL, putfiole, returngoodRessources, putpossition, grap_flask, terminatepirate, turnvilager, getDistanceBetweenPoints } from '../routes/unions.js';




describe('getDistanceBetweenPoints_true', () => {
	it('should calculate distance between two points', () => {
		const point1 = [48.858844, 2.294351];
		const point2 = [48.858844, 2.294351];
		const distance = getDistanceBetweenPoints(point1, point2);
		expect(distance).toBeLessThan(5);
	});
});

describe('getDistanceBetweenPoints_false', () => {
	it('should calculate distance between two points', () => {
		const point1 = [48.858844, 2.294351];
		const point2 = [48.858844, 4];
		const distance = getDistanceBetweenPoints(point1, point2);
		expect(distance).toBeGreaterThan(5);
	});
});


describe('decreaseTTl', () => {
	let GeoResource;

	beforeEach(() => {
		GeoResource = [
			{ role: 'ADMIN', ttl: 10 },
			{ role: 'FLASK', ttl: 5 },
			{ role: 'VILLAGEOIS', ttl: 3 }
		];
	});

	it('should decrease TTL for non-admin and non-flask resources', () => {
		decreaseTTl(GeoResource);
		expect(GeoResource[0].ttl).toEqual(10); // Admin resource TTL should remain unchanged
		expect(GeoResource[1].ttl).toEqual(5);  // Flask resource TTL should remain unchanged
		expect(GeoResource[2].ttl).toEqual(2);  // User TTL should decrease by 1
	});
});


describe('updateZRR', () => {
	let zrrData;

	beforeEach(() => {
		zrrData = {
			'limite-NO': [],
			'limite-NE': [],
			'limite-SE': [],
			'limite-SO': [],
			'ttl': 60
		};
	});

	it('should update ZRR data correctly', () => {
		const newZrr = {
			'limite-NO': [48.858844, 2.294351],
			'limite-NE': [48.858844, 2.294351],
			'limite-SE': [48.858844, 2.294351],
			'limite-SO': [48.858844, 2.294351]
		};

		// Faire une copie de l'objet zrrData avant l'appel à updateZRR
		const initialZrrData = { ...zrrData };

		// Appeler la fonction updateZRR
		updateZRR(zrrData, newZrr);

		// Vérifier que les propriétés de zrrData ont été correctement mises à jour
		expect(zrrData['limite-NO']).toEqual(newZrr['limite-NO']);
		expect(zrrData['limite-NE']).toEqual(newZrr['limite-NE']);
		expect(zrrData['limite-SE']).toEqual(newZrr['limite-SE']);
		expect(zrrData['limite-SO']).toEqual(newZrr['limite-SO']);
		expect(zrrData['ttl']).toEqual(initialZrrData['ttl']);

		// Vérifier que les autres propriétés de zrrData n'ont pas été modifiées
		expect(zrrData['ttl']).toEqual(initialZrrData['ttl']);
	});
});

describe('updateTTL', () => {
	let zrrData;

	beforeEach(() => {
		zrrData = {
			'limite-NO': [],
			'limite-NE': [],
			'limite-SE': [],
			'limite-SO': [],
			'ttl': 60
		};
	});

	it('should update TTL correctly', () => {
		const newTtl = 30;

		// Faire une copie de l'objet zrrData avant l'appel à updateTTL
		const initialZrrData = { ...zrrData };

		// Appeler la fonction updateTTL
		updateTTL(zrrData, newTtl);

		// Vérifier que la propriété ttl de zrrData a été correctement mise à jour
		expect(zrrData['ttl']).toEqual(newTtl);

		// Vérifier que les autres propriétés de zrrData n'ont pas été modifiées
		expect(zrrData['limite-NO']).toEqual(initialZrrData['limite-NO']);
		expect(zrrData['limite-NE']).toEqual(initialZrrData['limite-NE']);
		expect(zrrData['limite-SE']).toEqual(initialZrrData['limite-SE']);
		expect(zrrData['limite-SO']).toEqual(initialZrrData['limite-SO']);
	});
});

describe('putfiole', () => {
	let GeoResource;
	let zrrData;
	let body;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 'fiole1', role: 'FLASK', position: [48.858844, 2.294351], ttl: 5 },
			{ id: 'fiole2', role: 'FLASK', position: [48.858844, 2.294351], ttl: 10 }
		];

		zrrData = {
			ttl: 60,
			'limite-NO': [45.78265755880753, 4.8633331060409555],
			'limite-NE': [45.78265755880753, 4.867863357067109],
			'limite-SE': [45.78190934399971, 4.867863357067109],
			'limite-SO': [45.78190934399971, 4.8633331060409555]
		};

		body = {
			name: 'fiole2',
			position: [45.782064656548144, 4.865473528479445]
		};
	});

	it('should update existing fiole with new position and ttl', () => {
		putfiole(GeoResource, zrrData, body);

		// Vérifiez que la fiole existante a été mise à jour avec la nouvelle position et le nouveau ttl
		const updatedFiole = GeoResource.find(fiole => fiole.id === body.name);
		expect(updatedFiole.position).toEqual(body.position);
		expect(updatedFiole.ttl).toEqual(zrrData.ttl);
	});

	it('should add new fiole if not already existing', () => {
		const initialLength = GeoResource.length;
		const newFioleName = 'fiole3';
		body.name = newFioleName;

		putfiole(GeoResource, zrrData, body);

		// Vérifiez que la nouvelle fiole a été ajoutée à GeoResource
		expect(GeoResource.length).toEqual(initialLength + 1);
		const addedFiole = GeoResource.find(fiole => fiole.id === newFioleName);
		expect(addedFiole).toBeDefined();
		expect(addedFiole.role).toEqual('FLASK');
		expect(addedFiole.position).toEqual(body.position);
		expect(addedFiole.ttl).toEqual(zrrData.ttl);
	});

	it('should not add new fiole if position is outside ZRR', () => {
		body.name = 'fiole3';
		body.position = [40.7128, -74.0060]; // Position en dehors de la ZRR

		putfiole(GeoResource, zrrData, body);

		// Vérifiez que la nouvelle fiole n'a pas été ajoutée à GeoResource
		expect(GeoResource.find(fiole => fiole.id === body.name)).toBeUndefined();
	});
});


describe('returngoodRessources', () => {
	let GeoResource;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 1, role: 'ADMIN', name: 'Admin Resource' },
			{ id: 2, role: 'FLASK', name: 'Flask Resource' },
			{ id: 3, role: 'VILLAGEOIS', name: 'Villager Resource' }
		];
	});

	it('should return all resources for ADMIN role', () => {
		const role = 'ADMIN';
		const ressourceret = returngoodRessources(GeoResource, role);
		expect(ressourceret).toEqual(GeoResource); // Tous les resources doivent être retournés pour le rôle ADMIN
	});

	it('should return filtered resources for non-ADMIN roles', () => {
		const role = 'VILLAGEOIS';
		const ressourceret = returngoodRessources(GeoResource, role);
        
		// Seuls les resources correspondant au rôle spécifié ou à 'FLASK' doivent être retournés
		const expectedResources = GeoResource.filter(resource => resource.role === role || resource.role === 'FLASK');
		expect(ressourceret).toEqual(expectedResources);
	});
});


describe('putpossition', () => {
	let GeoResource;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 1, role: 'VILLAGEOIS', position: [48.858844, 2.294351], ttl: 0, potions: 0, terminated: 0, turned: 0 },
			{ id: 2, role: 'PIRATE', position: [45.4215, -75.6919], ttl: 0, potions: 0, terminated: 0, turned: 0 }
		];
	});

	it('should add new resource if ID not found in GeoResource', () => {
		const body = { position: [40.7128, -74.0060] };
		const idrequet = 3; // ID non existant dans GeoResource
	

	
		// Simuler l'appel à putpossition avec un nouvel ID
		putpossition(GeoResource, body, idrequet, 'VILLAGEOIS');
	

	
		// Vérifier que la nouvelle ressource a été ajoutée à GeoResource
		const newResource = GeoResource.find(resource => resource.id === idrequet);
		expect(newResource).toBeDefined(); // La nouvelle ressource doit être définie
		expect(newResource.position).toEqual(body.position); // La position de la nouvelle ressource doit correspondre à body.position
		expect(newResource.role).toEqual('VILLAGEOIS'); // Le rôle de la nouvelle ressource doit correspondre à 'VILLAGEOIS'
	});
	

	it('should update position of existing resource if ID found in GeoResource', () => {
		const body = { position: [37.7749, -122.4194] };
		const idrequet = 2; // ID existant dans GeoResource

		// Simuler l'appel à putpossition avec un ID existant
		putpossition(GeoResource, body, idrequet);

		// Vérifier que la position de la ressource existante a été mise à jour
		const updatedResource = GeoResource.find(resource => resource.id === idrequet);
		expect(updatedResource.position).toEqual(body.position); // La position de la ressource mise à jour doit correspondre à body.position
	});
});

describe('grap_flask', () => {
	let GeoResource;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 'user1', role: 'VILLAGEOIS', position: [48.858844, 2.294351], potions: 0, ttl: 10 },
			{ id: 'resource1', role: 'FLASK', position: [48.858844, 2.294351], ttl: 5 }
		];
	});

	it('should successfully grab a flask resource for a villageois user', () => {
		const id_user = 'user1';
		const id_resource = 'resource1';

		const result = grap_flask(GeoResource, id_user, id_resource);

		expect(result).toEqual(1); // Opération réussie
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(1); // Vérifier que la potion a été ajoutée à l'utilisateur
		expect(GeoResource.find(resource => resource.id === id_resource)).toBeUndefined(); // Vérifier que la ressource a été supprimée
	});

	it('should successfully grab a flask resource for a pirate user', () => {
		const id_user = 'user1';
		const id_resource = 'resource1';
		GeoResource[0].role = 'PIRATE'; // Changer le rôle de l'utilisateur

		const result = grap_flask(GeoResource, id_user, id_resource);

		expect(result).toEqual(1); // Opération réussie
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(1); // Vérifier que la potion a été ajoutée à l'utilisateur
		expect(GeoResource.find(resource => resource.id === id_resource)).toBeUndefined(); // Vérifier que la ressource a été supprimée
	});

	it('should return -2 when user is not a villageois or pirate', () => {
		const id_user = 'user1';
		const id_resource = 'resource1';
		GeoResource[0].role = 'FLASK'; // Changer le rôle de l'utilisateur

		const result = grap_flask(GeoResource, id_user, id_resource);

		expect(result).toEqual(-2); // L'opération est invalide pour ce type d'utilisateur
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(0); // Vérifier que la potion n'a pas été ajoutée
		expect(GeoResource.find(resource => resource.id === id_resource)).toBeDefined(); // Vérifier que la ressource n'a pas été supprimée
	});

	it('should return -3 when user is too far from the resource', () => {
		const id_user = 'user1';
		const id_resource = 'resource1';
		GeoResource[0].position = [40.7128, -74.0060]; // Changer la position de l'utilisateur (plus de 5 unités de distance)

		const result = grap_flask(GeoResource, id_user, id_resource);

		expect(result).toEqual(-3); // L'utilisateur est trop loin de la ressource
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(0); // Vérifier que la potion n'a pas été ajoutée
		expect(GeoResource.find(resource => resource.id === id_resource)).toBeDefined(); // Vérifier que la ressource n'a pas été supprimée
	});

	it('should return -1 when either user or resource is not found', () => {
		const id_user = 'non_existing_user';
		const id_resource = 'resource1';

		const result = grap_flask(GeoResource, id_user, id_resource);

		expect(result).toEqual(-1); // L'utilisateur ou la ressource n'a pas été trouvée
		expect(GeoResource.find(resource => resource.id === id_user)).toBeUndefined(); // Vérifier que l'utilisateur n'a pas été modifié
		expect(GeoResource.find(resource => resource.id === id_resource)).toBeDefined(); // Vérifier que la ressource n'a pas été supprimée
	});
});

describe('terminatepirate', () => {
	let GeoResource;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 'user1', role: 'VILLAGEOIS', position: [48.858844, 2.294351], potions: 2, terminated: 0 },
			{ id: 'user2', role: 'PIRATE', position: [48.858844, 2.294351] }
		];
	});

	it('should successfully terminate a pirate when conditions are met', () => {
		const id_user = 'user1';
		const id_user2 = 'user2';

		const result = terminatepirate(GeoResource, id_user, id_user2);

		expect(result).toEqual(1); // Opération réussie
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(1); // Vérifier que la potion a été consommée
		expect(GeoResource.find(resource => resource.id === id_user).terminated).toEqual(1); // Vérifier que le joueur a terminé un pirate
		expect(GeoResource.find(resource => resource.id === id_user2)).toBeUndefined(); // Vérifier que le pirate a été supprimé
	});

	it('should return -2 when conditions are not met for termination', () => {
		const id_user = 'user1';
		const id_user2 = 'user2';
		GeoResource[0].role = 'PIRATE'; // Changer le rôle de l'utilisateur


		const result = terminatepirate(GeoResource, id_user, id_user2);


		expect(result).toEqual(-2); // L'opération est invalide pour ces utilisateurs
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(2); // Vérifier que la potion n'a pas été consommée
		expect(GeoResource.find(resource => resource.id === id_user).terminated).toEqual(0); // Vérifier que le joueur n'a pas terminé de pirate
		expect(GeoResource.find(resource => resource.id === id_user2)).toBeDefined(); // Vérifier que le pirate n'a pas été supprimé

	});

	it('should return -3 when user is too far from the pirate', () => {
		const id_user = 'user1';
		const id_user2 = 'user2';
		GeoResource[0].position = [40.7128, -74.0060]; // Changer la position de l'utilisateur (plus de 5 unités de distance)

		const result = terminatepirate(GeoResource, id_user, id_user2);

		expect(result).toEqual(-3); // L'utilisateur est trop loin du pirate
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(2); // Vérifier que la potion n'a pas été consommée
		expect(GeoResource.find(resource => resource.id === id_user).terminated).toEqual(0); // Vérifier que le joueur n'a pas terminé de pirate
		expect(GeoResource.find(resource => resource.id === id_user2)).toBeDefined(); // Vérifier que le pirate n'a pas été supprimé
	});

	it('should return -1 when either user or pirate is not found', () => {
		const id_user = 'non_existing_user';
		const id_user2 = 'user2';

		const result = terminatepirate(GeoResource, id_user, id_user2);

		expect(result).toEqual(-1); // L'utilisateur ou le pirate n'a pas été trouvé
		// Vérifier que les ressources n'ont pas été modifiées
		expect(GeoResource.find(resource => resource.id === id_user)).toBeUndefined();
		expect(GeoResource.find(resource => resource.id === id_user2)).toBeDefined();
	});
});



describe('turnvilager', () => {
	let GeoResource;

	beforeEach(() => {
		// Initialisation des données pour chaque test
		GeoResource = [
			{ id: 'user1', role: 'PIRATE', position: [48.858844, 2.294351], potions: 1, ttl: 10, turned: 0 },
			{ id: 'user2', role: 'VILLAGEOIS', position: [48.858844, 2.294351] }
		];
	});

	it('should successfully turn a villager into a pirate', () => {
		const id_user = 'user1';
		const id_user2 = 'user2';

		const result = turnvilager(GeoResource, id_user, id_user2);

		expect(result).toEqual(1); // Opération réussie
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(0); // Vérifier que la potion a été utilisée
		expect(GeoResource.find(resource => resource.id === id_user2).role).toEqual('PIRATE'); // Vérifier que le rôle du villageois a été changé en pirate
		expect(GeoResource.find(resource => resource.id === id_user).turned).toEqual(1); // Vérifier que le compteur de transformations du pirate a été incrémenté
	});

	it('should return -2 when conditions for turning are not met', () => {
		const id_user = 'user1'; // Utilisateur déjà pirate
		const id_user2 = 'user2'; // Villageois
		GeoResource[0].potions = 0; // Changer le nombre de potions à 0

		const result = turnvilager(GeoResource, id_user, id_user2);

		expect(result).toEqual(-2); // L'opération est invalide pour ces utilisateurs
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(0); // Vérifier que la potion n'a pas été utilisée
		expect(GeoResource.find(resource => resource.id === id_user2).role).toEqual('VILLAGEOIS'); // Vérifier que le rôle du villageois n'a pas été modifié
		expect(GeoResource.find(resource => resource.id === id_user).turned).toEqual(0); // Vérifier que le compteur de transformations du pirate n'a pas été incrémenté
	});

	it('should return -3 when user is too far from the resource', () => {
		const id_user = 'user1'; // Pirate
		const id_user2 = 'user2'; // Villageois
		GeoResource[1].position = [40.7128, -74.0060]; // Changer la position du villageois (plus de 5 unités de distance)

		const result = turnvilager(GeoResource, id_user, id_user2);

		expect(result).toEqual(-3); // L'utilisateur est trop loin de la ressource
		expect(GeoResource.find(resource => resource.id === id_user).potions).toEqual(1); // Vérifier que la potion n'a pas été utilisée
		expect(GeoResource.find(resource => resource.id === id_user2).role).toEqual('VILLAGEOIS'); // Vérifier que le rôle du villageois n'a pas été modifié
		expect(GeoResource.find(resource => resource.id === id_user).turned).toEqual(0); // Vérifier que le compteur de transformations du pirate n'a pas été incrémenté
	});

	it('should return -1 when either user or resource is not found', () => {
		const id_user = 'non_existing_user'; // Utilisateur inexistant
		const id_user2 = 'user2'; // Villageois existant

		const result = turnvilager(GeoResource, id_user, id_user2);

		expect(result).toEqual(-1); // L'utilisateur ou le villageois n'a pas été trouvé
		// Vérifier que les données n'ont pas été modifiées
		expect(GeoResource.find(resource => resource.id === id_user2).role).toEqual('VILLAGEOIS'); // Vérifier que le rôle du villageois n'a pas été modifié
	});
});

