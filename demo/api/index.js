import express from 'express';
import './routes/websocket.js';

const app = express();
const port = 3376;
app.use(express.json());

import jeu from './routes/unions.js';

import auth_filter from './routes/auth/auth_filter.js';
import authenticateAdmin from './routes/auth/authenticateAdmin.js';

app.use('/api/', auth_filter);
app.use('/api/admin/', authenticateAdmin);

app.use('/api/', jeu);

app.get('/', (req, res) => {
	res.sendFile('index.html', { root: 'public' });
});

app.use('/static', express.static('public'));

app.use((req, res, next) => {
	res.status(404).send('Page introuvable');
	next();
});

app.use((err, req, res,next) => {
	console.error(err.stack);
	res.status(500).send('Something broke!');
	next();
});
  
app.listen(port, () => {
	console.log(`Example app listening on port ${port}`);
	console.log(`http://localhost:${port}/static/pageOne.html`);
});
