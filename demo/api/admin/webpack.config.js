// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
console.log('process.env.NODE_ENV:', process.env.NODE_ENV);
const isProduction = process.env.NODE_ENV === 'production';


const stylesHandler = 'style-loader';



const config = {
	entry: {
		indexOne: './src/pageOne/index.js',
		indexTwo: './src/pageTwo/index.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist')
	},
	devServer: {
		open: true,
		host: 'localhost'
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/pageOne/index.html',
			filename: 'pageOne.html',
			chunks: ['indexOne']
		}),
		new HtmlWebpackPlugin({
			template: './src/pageTwo/index.html',
			filename: 'pageTwo.html',
			chunks: ['indexTwo']
		})
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				loader: 'babel-loader'
			},
			{
				test: /\.css$/i,
				use: [stylesHandler,'css-loader']
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
				type: 'asset'
			},
			{
				test: /\.html$/,
				use: ['html-loader']
			}

			// Add your rules for custom modules here
			// Learn more about loaders from https://webpack.js.org/loaders/
		]
	},
	mode: isProduction ? 'production' : 'development'
};

module.exports = config;