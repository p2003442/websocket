let apiPath;

console.log('process.env.NODE_ENV:', process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
	apiPath = 'https://192.168.75.50/game';
} else {
	apiPath = 'http://localhost:3376';
}

export default apiPath;
