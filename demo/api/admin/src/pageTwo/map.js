
import apiPath from '../config.js';

var ws = new WebSocket('ws://localhost:3377');

// La connexion est ouverte
ws.addEventListener('open', (event) => {
	data = {message : 'coucou serveur'};
	ws.send(JSON.stringify(data));
});

ws.addEventListener('error', function (event) {
	console.log('Erreur WebSocket : ', event);
});

// Écoute les différents messages
ws.addEventListener('message', function (event) {
	//console.log('received: %s', event.data);
	const data = JSON.parse(event.data);

	data.forEach(element => {
		const existingMarker = findMarkerById(element.id);

		if (existingMarker) {
			existingMarker.setLatLng([element.position[0], element.position[1]]);
			existingMarker.getPopup().setContent(element.role === 'FLASK' ? 'FLASK' : element.id);
		} else {
			const popupContent = (element.role === 'FLASK') ? 'FLASK' : element.id;
			const newMarker = L.marker([element.position[0], element.position[1]])
				.addTo(mymap)
				.bindPopup(popupContent);
			newMarker.options.id = element.id;
		}
	});
});

/**
 *	Recherche un marker par son id
 * @param {string} id
 * @returns {L.Marker}
 */
function findMarkerById(id) {
	let foundMarker = null;
	mymap.eachLayer(layer => {
		if (layer instanceof L.Marker && layer.options.id === id) {
			foundMarker = layer;
		}
	});
	return foundMarker;
}

// initialisation de la map
const lat = 45.782, lng = 4.8656, zoom = 19;

const center = {
	latitude: 45.78207,
	longitude: 4.86559
};

// Rayon de déplacement autour du point central en degrés (à ajuster selon vos besoins)
const radius = 0.01; 

// Vitesse de déplacement (à ajuster selon vos besoins)
const speed = 0.002;

// Angle initial
let angle = 0;

// Intervalle de mise à jour de la position en millisecondes
const updateInterval = 0.001; // par exemple, toutes les 1 seconde

// Fonction pour mettre à jour la position de l'utilisateur et l'envoyer aux clients
function updatePosition() {
	const newPosition = [
		center.latitude + radius * Math.sin(angle),
		center.longitude + radius * Math.cos(angle)
	];

	angle += speed;

	if (angle >= 2 * Math.PI) {
		angle = 0;
	}

	const message = {
		type: 'put_pos',
		data: {
			id: '1',
			position: newPosition, 
			species: 'VILLAGEOIS'
		}
	};
	ws.send(JSON.stringify(message));
}

setInterval(updatePosition, updateInterval);

const mymap = L.map('map', {
	center: [lat, lng],
	zoom: zoom
});


async function get_georessource_and_add_marker() {
	const url = `${apiPath}/api/resources`;
	const token = localStorage.getItem('token');	
	const headers = {
		'Content-Type': 'application/json',
		'Authorization': token
	};
	const requestConfig = {
		method: 'GET',
		headers: headers
	};
	try {
		const response = await fetch(url, requestConfig);
		const data = await response.json();
		if (response.ok) {
			console.log('data:', data);
			data.forEach(element => {
				let popupContent;
				if (element.role === 'FLASK') {
					popupContent = 'FLASK';
				} else {
					popupContent = element.id;
				}

				L.marker([element.position[0], element.position[1]])
					.addTo(mymap)
					.bindPopup(popupContent);
			});
		} else {
			console.error('Erreur lors de la récupération des données GeoResource:', response.status);
		}
	} catch (err) {
		console.error('error:', err);
	}
}


// Mise à jour de la map
function updateMap(latlng, zoomp) {
	// Affichage à la nouvelle position
	mymap.setView(latlng, zoomp);
	//get_georessource_and_add_marker();
	// La fonction de validation du formulaire renvoie false pour bloquer le rechargement de la page.
	return false;
}




// Initialisation de la map
function initMap() {
	// Création d'un "tile layer" (permet l'affichage sur la carte)
	L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg90?access_token=pk.eyJ1IjoieGFkZXMxMDExNCIsImEiOiJjbGZoZTFvbTYwM29sM3ByMGo3Z3Mya3dhIn0.df9VnZ0zo7sdcqGNbfrAzQ', {
		maxZoom: 21,
		minZoom: 1,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1,
		accessToken: 'pk.eyJ1IjoibTFpZjEzIiwiYSI6ImNqczBubmhyajFnMnY0YWx4c2FwMmRtbm4ifQ.O6W7HeTW3UvOVgjCiPrdsA'
	}).addTo(mymap);

	// Ajout d'un marker
	L.marker([45.78207, 4.86559]).addTo(mymap).bindPopup('Entrée du bâtiment<br>Nautibus.').openPopup();	
	return mymap;
}


export { get_georessource_and_add_marker, updateMap };
export default initMap;