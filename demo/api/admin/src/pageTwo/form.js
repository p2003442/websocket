import apiPath from '../config.js';
import { updateMap } from './map.js';

// MàJ des inputs du formulaire
function updateLatValue(lat) {
	document.getElementById('lat').value = lat;
}

function updateLonValue(lng) {
	document.getElementById('lon').value = lng;
}

function updateZoomValue(zoom) {
	document.getElementById('zoom').value = zoom;
}

function setZrr(mymap) {
	const bounds = mymap.getBounds();
	const north = bounds.getNorth();
	const south = bounds.getSouth();
	const east = bounds.getEast();
	const west = bounds.getWest();
	document.getElementById('lon1').value = west;
	document.getElementById('lon2').value = east;
	document.getElementById('lat1').value = north;
	document.getElementById('lat2').value = south;
}

// Requêtes asynchrones
async function sendZrr(mymap) {
	//('TODO: send fetch request...');
	const url = `${apiPath}/api/admin/zrr`;
	const token = localStorage.getItem('token');	
	if (document.getElementById('lat1').value === '' || document.getElementById('lon1').value === '' || document.getElementById('lat2').value === '' || document.getElementById('lon2').value === '') {
		console.error('Error: missing values');
		return false;
	}
	const body = {
		zrr: {
			'limite-NO': [
				parseFloat(document.getElementById('lat1').value),
				parseFloat(document.getElementById('lon1').value)
			],
			'limite-NE': [
				parseFloat(document.getElementById('lat1').value),
				parseFloat(document.getElementById('lon2').value)
			],
			'limite-SE': [
				parseFloat(document.getElementById('lat2').value),
				parseFloat(document.getElementById('lon2').value)
			],
			'limite-SO': [
				parseFloat(document.getElementById('lat2').value),
				parseFloat(document.getElementById('lon1').value)
			]
		}
	};
	const headers = {
		'Content-Type': 'application/json',
		'Authorization': token	
	};
	const requestConfig = {
		method: 'PUT',
		headers: headers,
		body: JSON.stringify(body),
		mode: 'cors'
	};
	try {
		const response = await fetch(url, requestConfig);
		if (!response.ok) {
			const error = await response.json();
			throw new Error(error.error);
		} else {
			const lat1 = parseFloat(document.getElementById('lat1').value);
			const lon1 = parseFloat(document.getElementById('lon1').value);
			const lat2 = parseFloat(document.getElementById('lat2').value);
			const lon2 = parseFloat(document.getElementById('lon2').value);
			const bounds = [[lat1, lon1], [lat2, lon2]];
			if (mymap.zrrLayer) {
				mymap.removeLayer(mymap.zrrLayer);
			}
			mymap.zrrLayer = L.rectangle(bounds, {color: 'red'}).addTo(mymap);
			return true;
		}
	} catch (error) {
		console.error('Error:', error);
		return false;
	}
}

async function setTtl() {
	const ttl = document.getElementById('ttl').value;
	const url = `${apiPath}/api/admin/ttl`;
	const token = localStorage.getItem('token');
	const body = {
		ttl: ttl
	};
	const headers = {
		'Content-Type': 'application/json',
		'Authorization': token	
	};
	const requestConfig = {
		method: 'PUT',
		headers: headers,
		body: JSON.stringify(body)
	};
	try {
		const response = await fetch(url, requestConfig);
		if (!response.ok) {
			const error = await response.json();
			throw new Error(error.error);
		} else {
			return false;
		}
	} catch (error) {
		console.error('Error:', error);
		return false;
	}
}

async function ajouterPotion(lat, lng) {
	const url = `${apiPath}/api/admin/fiole`;
	const token = localStorage.getItem('token');
	const body = {
		name : `namePot${lat}x${lng}`,
		position: [lat, lng]
	};
	const headers = {
		'Content-Type': 'application/json',
		'Authorization': token
	};
	const requestConfig = {
		method: 'POST',
		headers: headers,
		body: JSON.stringify(body),
		mode: 'cors'
	};
	try {
		const response = await fetch(url, requestConfig);
		// recuperer l'erreur de l'api et l'afficher
		if (!response.ok) {
			const error = await response.json();
			throw new Error(error.error);
		} else {
			return false;
		}
	} catch (error) {
		console.error('Error:', error);
		return false;
	}
}


// Initialisation
function initListeners(mymap) {

	document.getElementById('zoom').addEventListener('change', e => {
		mymap.setZoom(e.target.value);
	});

	document.getElementById('lat').addEventListener('change', e => {
		mymap.panTo([e.target.value, mymap.getCenter().lng]);
	});

	document.getElementById('lon').addEventListener('change', e => {
		mymap.panTo([mymap.getCenter().lat, e.target.value]);
	});

	document.getElementById('setZrrButton').addEventListener('click', () => {
		setZrr(mymap);
	});

	document.getElementById('sendZrrButton').addEventListener('click', () => {
		sendZrr(mymap);
	});

	document.getElementById('setTtlButton').addEventListener('click', () => {
		setTtl();
	});

	document.getElementById('sendPotion').addEventListener('click', () => {
		const lat = document.getElementById('lat').value;
		const lon = document.getElementById('lon').value;
		ajouterPotion(lat, lon);
	});

	mymap.on('click', e => {
		updateMap([e.latlng.lat, e.latlng.lng], mymap.getZoom());
	});


	mymap.on('click', e => {
		updateLatValue(e.latlng.lat);
		updateLonValue(e.latlng.lng);
		updateZoomValue(mymap.getZoom());
	});

	mymap.on('zoomend', () => {
		updateZoomValue(mymap.getZoom());
	});
}


export { updateLatValue, updateLonValue, updateZoomValue };
export default initListeners;