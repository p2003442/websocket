import axios from 'axios';
import authenticateUser from './authenticate.js';

const auth_filter = (req, res, next) => {
	//console.log('auth_filter: req.headers:', req.headers);
	console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', req.hostname);
	if(req.headers.origin === undefined) {
		if (req.hostname === 'localhost'){
			req.headers.origin = 'http://localhost:3376';
		} else {
			req.headers.origin = req.protocol + 's://' + req.hostname;
		}
	}
	authenticateUser(req.headers.authorization, req.headers.origin)
		.then(response => {
			if (response.status === 200) {
				axios.get('http://192.168.75.50:8080/users/users/'+response.data.login, {
					headers: {
						Origin: req.headers.origin
					}
				}).then(response2 => {
					/* console.log('auth_filter: response.data:', response2.data);
					console.log('auth_filter1: req.data:', req.data); */
					req.data = {
						...req.data,
						login: response2.data.login,
						species: response2.data.species
					};
					//console.log('auth_filter: req.data:', req.data);
					next();
				}).catch(err => {
					console.error('Erreur lors de la requête de récupération de l utilisateur:', err);
					return res.status(401).json({ error: 'Erreur lors de la requête de récuoération de l\'utilisateur' });
				});
			} else {
				throw new Error('Unauthorized');
			}
		})
		.catch(err => {
			console.error('Erreur lors de la requête d\'authentification :', err);
			return res.status(401).json({ error: 'Erreur lors de la requête d\'authentification' });
		});
};

export default auth_filter;
