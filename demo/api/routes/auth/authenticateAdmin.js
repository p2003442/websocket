
function authenticateAdmin(req, res, next) {
	/* console.log('authenticateAdmin');
	console.log('req.data:', req.data); */
	if (req.data.species === 'ADMIN') {
		next();
	} else {
		return res.status(401).json({ error: 'Erreur lors de la requête d\'authentification (pas admin)' });
	}
						
}


export default authenticateAdmin;