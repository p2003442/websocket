import axios from 'axios';

function authenticateUser(token, origin) {
	console.log('authenticateUser: token:', token);
	console.log('authenticateUser: origin:', origin);
	return axios.get('http://192.168.75.50:8080/users/authenticate', {
		params: {
			jwt: token,
			origin: origin
		}
	});
}


export default authenticateUser;