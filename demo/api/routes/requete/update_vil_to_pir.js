import axios from 'axios';

export function update_vil_to_pir(id_vil,Origin) {
	console.log('In methods update_vil_to_pir');
	axios.put('http://192.168.75.50:8080/users/users/'+id_vil, {
		species: 'PIRATE'
	}, {
		Origin: Origin
	}).then(response => {
		if (response.status === 201 || response.status === 204) {
			console.log('Le villageois a été transformé en pirate avec succès.');
			return true;
		} else {
			console.error('Erreur lors de la requête de transformation du villageois en pirate:', response.status);
			return false;
		}
	}).catch(err => {
		console.error('Erreur lors de la requête de transformation du villageois en pirate:', err);
		return false;
	});
}