import axios from 'axios';

export function suppr_pirate(id_pirrate,Origin) {
	console.log('In methods suppr_pirate');
	axios.delete('http://192.168.75.50:8080/users/users/'+id_pirrate, {
		Origin: Origin
	}).then(response => {
		if (response.status === 204) {
			console.log('Le pirate a été supprimé avec succès.');
			return true;
		} else {
			console.error('Erreur lors de la requête de suppression du pirate:', response.status);
			return false;
		}
	}).catch(err => {
		console.error('Erreur lors de la requête de récupération de l utilisateur:', err);
		return false;
	});
}

