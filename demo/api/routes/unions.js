import express from 'express';
import { suppr_pirate } from './requete/suppr_pirate.js';
import { update_vil_to_pir } from './requete/update_vil_to_pir.js';
import { GeoResourceUpdated } from './websocket.js';

export const GeoResource = [];
const zrrData = {
	'limite-NO': [],
	'limite-NE': [],
	'limite-SE': [],
	'limite-SO': [],
	'ttl': 60
};

const router = express.Router();


export function getDistanceBetweenPoints([latitude1, longitude1], [latitude2, longitude2]) {
	const theta = longitude1 - longitude2;
	const distance = 60 * 1.1515 * 1.609344 * 1000 * (180/Math.PI) * Math.acos(
		Math.sin(latitude1 * (Math.PI/180)) * Math.sin(latitude2 * (Math.PI/180)) + 
        Math.cos(latitude1 * (Math.PI/180)) * Math.cos(latitude2 * (Math.PI/180)) * Math.cos(theta * (Math.PI/180))
	);

	return Math.round(distance, 2);
}




export const decreaseTTl = (GeoResource) => {
	// si ce n'est pas un admin ou une flask je decrease le ttl
	let modif = false;
	GeoResource = GeoResource.forEach(resource => {
		if (resource.role !== 'ADMIN' && resource.role !== 'FLASK') {
			if(resource.ttl > 0) {
				resource.ttl -= 1;
				modif = true;
			}
		}
	});
	if(modif) {
		GeoResourceUpdated();
	}
};



export const updateZRR = (zrrStock,Newzrr) => {
	zrrStock['limite-NO'] = Newzrr['limite-NO'];
	zrrStock['limite-NE'] = Newzrr['limite-NE'];
	zrrStock['limite-SE'] = Newzrr['limite-SE'];
	zrrStock['limite-SO'] = Newzrr['limite-SO'];
};

export const updateTTL = (zrrStock,ttl) => {
	zrrStock['ttl'] = ttl;
};


// Route pour fixer le périmètre de jeu (ZRR)
router.put('/admin/zrr', (req, res) => {
	console.log('PUT /admin/zrr');
	const body = req.body;
	if (!body || !body.zrr) {
		return res.status(400).json({ error: 'Le corps de la requête est vide ou ne contient pas de propriété \'zrr\'' });
	}
	const zrr = body.zrr;
	updateZRR(zrrData, zrr);
	console.log('La ZRR a été mise à jour avec succès.');
	return res.status(204).json({ message: 'La ZRR a été mise à jour avec succès.' });
});

// Route pour préciser le TTL initial
router.put('/admin/ttl', (req, res) => {
	console.log('PUT /admin/ttl');
	const body = req.body;
	if (!body || !body.ttl) {
		return res.status(400).json({ error: 'Le corps de la requête est vide ou ne contient pas de propriété \'ttl\'' });
	}
	const ttl = body.ttl;
	updateTTL(zrrData, ttl);
	console.log('Le TTL a été mis à jour avec succès.');
	return res.status(204).json({ message: 'Le TTL a été mis à jour avec succès.' });
});

export const putfiole = (GeoResource,zrrData, body) => {
	const ttl = zrrData.ttl;
	const existingFioleIndex = GeoResource.findIndex(fiole => fiole.id === body.name);
	const fiole = body.position;

	if (fiole[0] > zrrData['limite-NO'][0] || fiole[0] < zrrData['limite-SO'][0] || fiole[1] > zrrData['limite-NE'][1] || fiole[1] < zrrData['limite-SO'][1]) {
		return -1;
	}

	if (existingFioleIndex !== -1) {
		GeoResource[existingFioleIndex].position = body.position;
		GeoResource[existingFioleIndex].ttl = ttl;
		GeoResourceUpdated();
	} else {
		const newFiole = {
			id: body.name,
			role : 'FLASK',
			position: body.position,
			ttl: ttl
		};
		GeoResource.push(newFiole);
		GeoResourceUpdated();
	}
};


// Route pour déclencher l'apparition d'une fiole
router.post('/admin/fiole', (req, res) => {
	console.log('POST /admin/fiole');
	const body = req.body;

	if (!body || !body.name || !body.position) {
		return res.status(400).json({ error: 'Le corps de la requête est vide ou ne contient pas de propriété \'name\' ou \'position\'' });
	}

	// Logique pour ajouter une fiole

	const retour = putfiole(GeoResource,zrrData, body);
	if (retour === -1) {
		return res.status(400).json({ error: 'La fiole doit être placée dans la ZRR' });
	}

	console.log('La fiole a été ajoutée avec succès.');
	return res.status(204).json({ message: 'La fiole a été ajoutée avec succès.' });
});



// Route pour diminuer le TTL des ressources non admin ou fiole
setInterval(() => {
	decreaseTTl(GeoResource);
}, 1000);

export const returngoodRessources = (GeoResource,role) => {
	let ressourceret;
	if (role !== 'ADMIN') {
		ressourceret = GeoResource.filter(resource => resource.role === role || resource.role === 'FLASK');
	} else {
		ressourceret = GeoResource;
	}
	return ressourceret;
};


// Route pour récupérer toutes les ressources
// router.get('/resources', (req, res) => {
// 	console.log('GET /resources');
// 	console.log('GeoResource:', GeoResource);
// 	console.log('zrrData:', zrrData);
// 	const role = req.data.species;
// 	let ressourceret;
// 	ressourceret = returngoodRessources(GeoResource,role);
// 	return res.status(200).json(ressourceret);
// });

/*
export const putpossition = (GeoResource,body,idrequet,species) => {
	//console.log('In methods putpossition');
	const position = body.position;
	const resourceIndex = GeoResource.findIndex(resource => resource.id === idrequet);
	if (resourceIndex === -1) {
		let newResource;
		if (species === 'VILLAGEOIS') {
			newResource = {
				id: idrequet,
				role: 'VILLAGEOIS',
				position: position,
				ttl: 0,
				potions: 0,
				terminated: 0,
				turned : 0
			};
		} else if (species === 'PIRATE') {
			newResource = {
				id: idrequet,
				role: 'PIRATE',
				position: position,
				ttl: 0,
				potions: 0,
				terminated: 0,
				turned: 0
			};
		}
		GeoResource.push(newResource);
		GeoResourceUpdated();
	} else {
		GeoResource[resourceIndex].position = position;
		GeoResourceUpdated();
	}

};

// Route pour modifier la position d'une ressource
router.put('/resources/:id/position', (req, res) => {
	console.log('PUT /admin/resources/:id/position');
	const body = req.body;
	const idrequet = req.params.id;
	const species = req.data.species;

	if (!body || !body.position) {
		return res.status(400).json({ error: 'Le corps de la requête est vide ou ne contient pas de propriété \'position\'' });
	}

	putpossition(GeoResource,body,idrequet,species);

	console.log('Les données de la ressource ont été mises à jour avec succès.');
	return res.status(204).json({ message: 'Les données de la ressource ont été mises à jour avec succès.' });
});
*/
// Route pour effectuer différentes opérations sur les ressources
router.post('/resources/:id', (req, res) => {
	console.log('POST /admin/resources/:id');
	const idrequet = req.params.id;
	const body = req.body;
	const loginFromResponse = req.data.login;
	if (body.op === 'grab potion flask') {
		grab_potion_flask(loginFromResponse, idrequet, res);
	} else if (body.op === 'terminate pirate') {
		terminate_pirate(loginFromResponse, idrequet, res, req);
	} else if (body.op === 'turn villager into pirate') {
		turn_villager_into_pirate(loginFromResponse, idrequet, res, req);
	} else {
		return res.status(400).json({ error: 'Invalid operation type or resource is not operable by user at this moment' });
	}
});

// Route pour récupérer les données de la ZRR
router.get('/zrr', (req, res) => {
	console.log('GET /admin/zrr');
	const zrr = {
		'limite-NO': zrrData['limite-NO'],
		'limite-NE': zrrData['limite-NE'],
		'limite-SE': zrrData['limite-SE'],
		'limite-SO': zrrData['limite-SO']
	};
	res.status(200).json(zrr);
});
export const grap_flask = (GeoResource, id_user, id_resource) => {
	const resourceIndexJoueur = GeoResource.findIndex(resource => resource.id === id_user);
	const resourceIndexRessource = GeoResource.findIndex(resource => resource.id === id_resource);
    
	if (resourceIndexJoueur === -1 || resourceIndexRessource === -1) {
		return -1;
	}

	const userRole = GeoResource[resourceIndexJoueur].role;
	const resourceRole = GeoResource[resourceIndexRessource].role;

	// Vérifier si l'utilisateur est un villageois, un pirate ou une ressource est une potion
	if ((userRole !== 'VILLAGEOIS' && userRole !== 'PIRATE') || resourceRole !== 'FLASK') {
		return -2;
	}

	// Vérifier la distance entre l'utilisateur et la ressource
	if (getDistanceBetweenPoints(GeoResource[resourceIndexJoueur].position, GeoResource[resourceIndexRessource].position) > 5) {
		return -3;
	}

	// Ajouter une potion à l'utilisateur et mettre à jour TTL
	GeoResource[resourceIndexJoueur].potions += 1;
	GeoResource[resourceIndexJoueur].ttl = GeoResource[resourceIndexRessource].ttl;

	// Supprimer la ressource de la liste GeoResource
	GeoResource.splice(resourceIndexRessource, 1);

	return 1; // Opération réussie
};


function grab_potion_flask(id_user, id_resource, res) {
	console.log('In methods grab_potion_flask');
	const retour = grap_flask(GeoResource,id_user,id_resource);
	if (retour === -1) {
		return res.status(404).json({ error: 'Resource not found' });
	}
	if (retour === -2) {
		return res.status(400).json({ error: 'Invalid operation type or resource is not operable by user at this moment' });
	}

	if (retour === -3) {
		return res.status(403).json({ error: 'User too far from resource to grab it' });
	}

	console.log('Les données de GeoResource ont été mises à jour avec succès.');
	return res.status(200).json({ message: 'Resource grabbed' });
}

export const terminatepirate = (GeoResource,id_user, id_user2) => {
	const resourceIndexJoueur1 = GeoResource.findIndex(resource => resource.id === id_user);
	const resourceIndexJoueur2 = GeoResource.findIndex(resource => resource.id === id_user2);
	if (resourceIndexJoueur1 === -1 || resourceIndexJoueur2 === -1) {
		return -1;
	}

	if (GeoResource[resourceIndexJoueur1].role !== 'VILLAGEOIS' || GeoResource[resourceIndexJoueur1].potions < 1 || GeoResource[resourceIndexJoueur2].role !== 'PIRATE') {
		return -2;
	}
	if (getDistanceBetweenPoints(GeoResource[resourceIndexJoueur1].position, GeoResource[resourceIndexJoueur2].position) > 5) {
		return -3;
	}
	GeoResource[resourceIndexJoueur1].potions -= 1;
	GeoResource[resourceIndexJoueur1].terminated += 1;

	// Supprimer le pirate de GeoResource
	GeoResource.splice(resourceIndexJoueur2, 1);
	return 1;
};

function terminate_pirate(id_user, id_user2, res, req) {
	console.log('In methods terminate_pirate');
	const retour = terminatepirate(GeoResource,id_user, id_user2);
	if (retour === -1) {
		return res.status(404).json({ error: 'Resource not found' });
	}

	if (retour === -2) {
		return res.status(400).json({ error: 'Invalid operation type or resource is not operable by user at this moment' });
	}
	if (retour === -3) {
		return res.status(403).json({ error: 'User too far from resource to grab it' });
	}
	
	if (retour === 1) {
		console.log('Les données de GeoResource ont été mises à jour avec succès.');
		suppr_pirate(id_user2, req.headers.Origin);
	}

	return res.status(200).json({ message: 'Pirate terminated' });
}

export const turnvilager = (GeoResource, id_user, id_user2) => {
	const resourceIndexJoueur1 = GeoResource.findIndex(resource => resource.id === id_user);
	const resourceIndexJoueur2 = GeoResource.findIndex(resource => resource.id === id_user2);

	if (resourceIndexJoueur1 === -1 || resourceIndexJoueur2 === -1) {
		return -1; // Ressource non trouvée
	}

	const joueur1 = GeoResource[resourceIndexJoueur1];
	const joueur2 = GeoResource[resourceIndexJoueur2];

	if (joueur1.role !== 'PIRATE' || joueur1.potions < 1 || joueur2.role !== 'VILLAGEOIS') {
		return -2; // Conditions pour la transformation non remplies
	}

	const distance = getDistanceBetweenPoints(joueur1.position, joueur2.position);
	if (distance > 5) {
		return -3; // Distance trop grande entre les joueurs
	}

	// Effectuer la transformation
	GeoResource[resourceIndexJoueur1].potions -= 1;
	GeoResource[resourceIndexJoueur1].turned += 1;
	GeoResource[resourceIndexJoueur2].role = 'PIRATE';

	return 1; // Transformation réussie
};


function turn_villager_into_pirate(id_user, id_user2, res, req) {
	console.log('In methods turn_villager_into_pirate');
	const retour = turnvilager(GeoResource,id_user, id_user2);
	if (retour === -1) {
		return res.status(404).json({ error: 'Resource not found' });
	}

	if (retour === -2) {
		return res.status(400).json({ error: 'Invalid operation type or resource is not operable by user at this moment' });
	}

	if (retour === -3) {
		return res.status(403).json({ error: 'User too far from resource to grab it' });
	}

	if (retour === 1) {
		console.log('Les données de GeoResource ont été mises à jour avec succès.');
		update_vil_to_pir(id_user2, req.headers.Origin);
	}
	return res.status(200).json({ message: 'Villager turned into pirates' });
}


export default router;
