import { WebSocketServer } from 'ws';
import { GeoResource } from './unions.js';

const ws = new WebSocketServer({ port: 3377 });

ws.on('connection', function connection(ws) {
	ws.on('error', console.error);

	ws.on('message', function message(data) {
		console.log('received: %s', data);
		data = JSON.parse(data);
		if (data.type == 'put_pos') {
			console.log('put_data');
			try {
				const position = data.data.position;
				const idrequet = data.data.id;
				const species = data.data.species;
				const resourceIndex = GeoResource.findIndex(resource => resource.id === data.data.id);
				if (resourceIndex === -1) {
					let newResource;
					if (species === 'VILLAGEOIS') {
						newResource = {
							id: idrequet,
							role: 'VILLAGEOIS',
							position: position,
							ttl: 0,
							potions: 0,
							terminated: 0,
							turned : 0
						};
					} else if (species === 'PIRATE') {
						newResource = {
							id: idrequet,
							role: 'PIRATE',
							position: position,
							ttl: 0,
							potions: 0,
							terminated: 0,
							turned: 0
						};
					}
					GeoResource.push(newResource);
				} else {
					GeoResource[resourceIndex].position = position;
				}
			} catch (err) {
				console.error('error:', err);
			}
			ws.send(JSON.stringify(GeoResource));
		}
	});
	ws.send(JSON.stringify(GeoResource));
});

/**
 * Envoie la georessource update à tous les clients connectés
 */
export function GeoResourceUpdated() {
	ws.clients.forEach(function each(w) {
		if (w.isAlive === false) return w.terminate();

		w.send(JSON.stringify(GeoResource));
	});
}

export default ws;