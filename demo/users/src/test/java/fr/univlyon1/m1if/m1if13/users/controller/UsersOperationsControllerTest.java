package fr.univlyon1.m1if.m1if13.users.controller;

import fr.univlyon1.m1if.m1if13.users.dao.UserDao;
import fr.univlyon1.m1if.m1if13.users.model.Species;
import fr.univlyon1.m1if.m1if13.users.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UsersOperationsControllerTest {
        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private UserDao userDao;

        private String jwt;

        @BeforeEach
        void setUp() throws Exception {
            User testUser = new User("titi", Species.VILLAGEOIS, "abc");
            userDao.save(testUser);

            User testUser2 = new User("alogout", Species.VILLAGEOIS, "alogout");
            userDao.save(testUser2);
            MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "alogout")
                            .param("password", "alogout")
                            .header("Origin", "https://192.168.75.50"))
                    .andReturn();
            jwt = result.getResponse().getHeader("Authorization");
            System.out.println(jwt);

        }

        /** ----------------- Test de la méthode login ----------------- **/

        /*
        * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginURLOrigin1() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("login", "titi")
                    .param("password", "abc")
                    .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginURLOrigin2() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "titi")
                            .param("password", "abc")
                            .header("Origin", "http://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginURLOrigin3() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "titi")
                            .param("password", "abc")
                            .header("Origin", "http://localhost"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire JSON
         */
        @Test
        void loginJSONOrigin1() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"login\":\"titi\",\"password\":\"abc\"}")
                    .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire JSON
         */
        @Test
        void loginJSONOrigin2() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"login\":\"titi\",\"password\":\"abc\"}")
                            .header("Origin", "http://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire JSON
         */
        @Test
        void loginJSONOrigin3() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"login\":\"titi\",\"password\":\"abc\"}")
                            .header("Origin", "http://localhost"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginWrongOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "titi")
                            .param("password", "abc")
                            .header("Origin", "https://tinyurl.com/veaknf46"))
                    .andExpect(status().isForbidden());
        }



        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginWrongPassword() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "titi")
                            .param("password", "wrong")
                            .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isUnauthorized());
        }

        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void loginNotFound() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "toto1")
                            .param("password", "abc1")
                            .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNotFound());
        }

        /*
         * Test de la méthode login avec un formulaire urlencoded
         */
        @Test
        void NotOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/login")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "toto")
                            .param("password", "abc"))
                    .andExpect(status().isBadRequest());
        }

        /** ----------------- Test de la méthode logout ----------------- **/

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutURLOrgine1() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("login", "alogout")
                    .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutURLOrgine2() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "alogout")
                            .header("Origin", "http://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutURLOrgine3() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "alogout")
                            .header("Origin", "http://localhost"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode logout avec un formulaire JSON
         */
        @Test
        void logoutJSONOrigine1() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"login\":\"alogout\"}")
                    .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode logout avec un formulaire JSON
         */
        @Test
        void logoutJSONOrigine2() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"login\":\"alogout\"}")
                            .header("Origin", "http://192.168.75.50"))
                    .andExpect(status().isNoContent());
        }

        /*
         * Test de la méthode logout avec un formulaire JSON
         */
        @Test
        void logoutJSONOrigine3() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"login\":\"alogout\"}")
                            .header("Origin", "http://localhost"))
                    .andExpect(status().isNoContent());
        }


        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutWrongOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "alogout")
                            .header("Origin", "https://tinyurl.com/veaknf46"))
                    .andExpect(status().isForbidden());
        }

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutNotFound() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "toto")
                            .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isNotFound());
        }

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutNotOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "toto"))
                    .andExpect(status().isBadRequest());
        }

        /*
         * Test de la méthode logout avec un formulaire urlencoded
         */
        @Test
        void logoutNotLogin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/users")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("login", "toto")
                    .param("password", "abc")
                    .param("species", "VILLAGEOIS"))
                    .andExpect(status().isCreated());
            mockMvc.perform(MockMvcRequestBuilders.post("/logout")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("login", "toto")
                            .header("Origin", "https://192.168.75.50"))
                    .andExpect(status().isUnauthorized());
        }

        /** ----------------- Test de la méthode Authenticate ----------------- **/

        /*
         * Test de la méthode authenticate
         */
        @Test
        void authenticate() throws Exception {

            mockMvc.perform(MockMvcRequestBuilders.get("/authenticate")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("origin", "https://192.168.75.50")
                    .param("jwt", jwt))
                    .andExpect(status().isOk());
        }

        /*
         * Test de la méthode authenticate
         */
        @Test
        void authenticateWrongOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get("/authenticate")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .param("origin", "https://tinyurl.com/veaknf46")
                    .param("jwt", jwt))
                    .andExpect(status().isUnauthorized());
        }

        /*
         * Test de la méthode authenticate
         */
        @Test
        void authenticateWrongToken() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get("/authenticate")
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("origin", "https://192.168.75.50")
                            .param("jwt", "uhgaqufzpihgrzapugfvfaogefvaurgfazfgyauykfgafupigvza"))
                    .andExpect(status().isUnauthorized());
        }

        /*
         * Test de la méthode authenticate
         */
        @Test
        void authenticateNotOrigin() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get("/authenticate")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .header("Authorization", jwt))
                    .andExpect(status().isBadRequest());
        }

}
