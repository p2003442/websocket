package fr.univlyon1.m1if.m1if13.users.controller;

import fr.univlyon1.m1if.m1if13.users.dao.UserDao;
import fr.univlyon1.m1if.m1if13.users.model.Species;
import fr.univlyon1.m1if.m1if13.users.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class is the REST controller test.
 */
@SpringBootTest
@AutoConfigureMockMvc
public class RESTControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserDao userDao;
    @Test
    public void contextLoads() {
        userDao = mock(UserDao.class);
    }

    /**
     * Test the get users method with JSON.
     * @throws Exception
     */

    @Test
    public void testGetUserJSON() throws Exception {
        this.mockMvc.perform(get("/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/json"));
    }

    /**
     * Test the get users method with XML.
     * @throws Exception
     */
    @Test
    public void testGetUserXML() throws Exception {
        this.mockMvc.perform(get("/users")
                .accept(MediaType.APPLICATION_XML))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/xml"));
    }

    /**
     * Test the get users method with HTML.
     * @throws Exception
     */
    @Test
    public void testGetUserHTML() throws Exception {
        this.mockMvc.perform(get("/users")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "text/html;charset=UTF-8"));
    }

    /**
     *  --------------------------Test the post creator user method. --------------------------
     */

    /**
     * Test the post creator json user method.
     * @throws Exception
     */
    @Test
    public void testCreateUserJSON() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoJSON\", \"species\": \"PIRATE\", \"password\": \"123\"}"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "users/totoJSON"));
    }


    /**
     * Test the post creator form user method.
     * @throws Exception
     */
    @Test
    public void testCreateUserForm() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "totoForm")
                .param("species", "PIRATE")
                .param("password", "123"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "users/totoForm"));
    }

    /**
     * Test the post creator json with conflict user method.
     * @throws Exception
     */
    @Test
    public void testCreateConflitJSONCreateUser() throws Exception {
        User testUser = new User("totoConflit", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoConflit\", \"species\": \"PIRATE\", \"password\": \"123\"}"))
                .andExpect(status().isConflict());
    }

    /**
     * Test the post creator form with conflict user method.
     * @throws Exception
     */
    @Test
    public void testCreateConflitFormCreateUser() throws Exception {
        User testUser = new User("totoConflitForm", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "totoConflitForm")
                .param("species", "PIRATE")
                .param("password", "123"))
                .andExpect(status().isConflict());
    }

    /**
     * Test the post creator json with bad request (without species) user method.
     * @throws Exception
     */
    @Test
    public void testNotSpeciesJSONCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoNotSpecies\", \"password\": \"123\"}"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the post creator form with bad request (without species) user method.
     * @throws Exception
     */
    @Test
    public void testNotSpeciesFormCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "totoNotSpeciesForm")
                .param("password", "123"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the post creator json with bad request (invalid species) user method.
     * @throws Exception
     */
    @Test
    public void testInvalidSpeciesJSONCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoInvalidSpecies\", \"species\": \"AAA\", \"password\": \"123\"}"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the post creator form with bad request (invalid species) user method.
     * @throws Exception
     */
    @Test
    public void testInvalidSpeciesFormCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "totoInvalidSpeciesForm")
                .param("species", "AAA")
                .param("password", "123"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the post creator json with bad request (without password) user method.
     * @throws Exception
     */
    @Test
    public void testNotPasswordJSONCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoNotPassword\", \"species\": \"PIRATE\"}"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the post creator form with bad request (without password) user method.
     * @throws Exception
     */
    @Test
    public void testNotPasswordFormCreateUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", "totoNotPasswordForm")
                .param("species", "PIRATE"))
                .andExpect(status().isBadRequest());
    }

    /**
     * --------------------------Test the get user/{login} method. --------------------------
     */

    /**
     * Test the get user with login JSON method.
     * @throws Exception
     */
    @Test
    public void testGetUserJsonLoginOrigine1() throws Exception {
        User testUser = new User("totoaaaaJSON", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaJSON")
                        .header("Origin", "http://localhost")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/json"));
    }

    /**
     * Test the get user with login JSON method.
     * @throws Exception
     */
    @Test
    public void testGetUserJsonLoginOrigine2() throws Exception {
        User testUser = new User("totoaaaaJSON", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaJSON")
                        .header("Origin", "http://192.168.75.50")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/json"));
    }

    /**
     * Test the get user with login JSON method.
     * @throws Exception
     */
    @Test
    public void testGetUserJsonLoginOrigine3() throws Exception {
        User testUser = new User("totoaaaaJSON", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaJSON")
                        .header("Origin", "https://192.168.75.50")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/json"));
    }

    /**
     * Test the get user with login JSON method.
     * @throws Exception
     */
    @Test
    public void testGetUserJsonWrongOrigine() throws Exception {
        User testUser = new User ("totoaaaaJSON", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaJSON")
                        .header("Origin", "https://tinyurl.com/veaknf46")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    /**
     * Test the get user with login XML method.
     * @throws Exception
     */
    @Test
    public void testGetUserXMLLogin() throws Exception {
        User testUser = new User("totoaaaaXML", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaXML")
                .accept(MediaType.APPLICATION_XML))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "application/xml"));
    }

    /**
     * Test the get user with login HTML method.
     * @throws Exception
     */
    @Test
    public void testGetUserHTMLLogin() throws Exception {
        User testUser = new User("totoaaaaHTML", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(get("/users/totoaaaaHTML")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type", "text/html;charset=UTF-8"));
    }

    /**
     * Test the get user with login not found JSON method.
     * @throws Exception
     */
    @Test
    public void testGetUserNotFoundHTML() throws Exception {
        this.mockMvc.perform(get("/users/totoaaaaHTMLNotFound")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isNotFound());
    }

    /**
     * Test the get user with login not found XML method.
     * @throws Exception
     */
    @Test
    public void testGetUserNotFoundJSON() throws Exception {
        this.mockMvc.perform(get("/users/totoaaaaJSONNotFound")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    /**
     * Test the get user with login not found XML method.
     * @throws Exception
     */
    @Test
    public void testGetUserNotFoundXML() throws Exception {
        this.mockMvc.perform(get("/users/totoaaaaXMLNotFound")
                .accept(MediaType.APPLICATION_XML))
                .andExpect(status().isNotFound());
    }

    /**
     * --------------------------Test the put user/{login} method. --------------------------
     */
    /**
     * Test the put user password JSON method.
     * @throws Exception
     */
    @Test
    public void testPutUserPasswordJSON() throws Exception {
        User testUser = new User("totoPutPassword", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutPassword")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"password\": \"1234\"}"))
                .andExpect(status().isNoContent());
        // verifier que dans la reponse de la requete userpassword = 1234
        this.mockMvc.perform(get("/users/totoPutPassword")
                .content("{\"password\": \"1234\"}"));
    }

    /**
     * Test the put user password form method.
     * @throws Exception
     */
    @Test
    public void testPutUserPasswordForm() throws Exception {
        User testUser = new User("totoPutPasswordForm", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutPasswordForm")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("password", "1234"))
                .andExpect(status().isNoContent());
        // verifier que dans la reponse de la requete userpassword = 1234
        this.mockMvc.perform(get("/users/totoPutPasswordForm")
                .content("{\"password\": \"1234\"}"));
    }

    /**
     * Test the put user not found form method.
     * @throws Exception
     */
    @Test
    public void testPutUserSpeciesJSON() throws Exception {
        User testUser = new User("totoPutSpecies", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutSpecies")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"species\": \"VILLAGEOIS\"}"))
                .andExpect(status().isNoContent());
        // verifier que dans la reponse de la requete userspecies = NINJA
        this.mockMvc.perform(get("/users/totoPutSpecies")
                .content("{\"species\": \"VILLAGEOIS\"}"));
    }

    /**
     * Test the put user not found form method.
     * @throws Exception
     */
    @Test
    public void testPutUserSpeciesForm() throws Exception {
        User testUser = new User("totoPutSpeciesForm", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutSpeciesForm")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("species", "VILLAGEOIS"))
                .andExpect(status().isNoContent());
        // verifier que dans la reponse de la requete userspecies = NINJA
        this.mockMvc.perform(get("/users/totoPutSpeciesForm")
                .content("{\"species\": \"VILLAGEOIS\""));
    }

    /**
     * Test the put user not found form method.
     * @throws Exception
     */
    @Test
    public void testPutUserCreatedFORM() throws Exception {
        this.mockMvc.perform(put("/users/totoPutNotFound")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("species", "VILLAGEOIS")
                .param("password", "1234"))
                .andExpect(status().isCreated());
    }

    /**
     * Test the put user not found json method.
     * @throws Exception
     */
    @Test
    public void testPutUserCreatedJSON() throws Exception {
        this.mockMvc.perform(put("/users/totoPutNotFoundJSON")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"species\": \"PIRATE\", \"password\": \"123\"}"))
                .andExpect(status().isCreated());
    }

    /**
     * Test the put user not found json method.
     * @throws Exception
     */
    @Test
    public void testPutUserBadRequestJSON() throws Exception {
        User testUser = new User("totoPutBadRequestJSON", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutBadRequestJSON")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"species\": \"NINJAAAAAAAA\"}"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test the put user not found form method.
     * @throws Exception
     */
    @Test
    public void testPutUserBadRequestForm() throws Exception {
        User testUser = new User("totoPutBadRequestForm", Species.VILLAGEOIS, "abc");
        userDao.save(testUser);
        this.mockMvc.perform(put("/users/totoPutBadRequestForm")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("species", "NINJAAAAAAAA"))
                .andExpect(status().isBadRequest());
    }




    /**
     * --------------------------Test the delete user/{login} method. --------------------------
     */

    /**
     * Test the delete user method.
     * @throws Exception
     */
    @Test
    public void testDeleteUser() throws Exception {
        this.mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\": \"totoDelete\", \"species\": \"PIRATE\", \"password\": \"123\"}"))
                .andExpect(status().isCreated());
        // @DeleteMapping(value = "/users/{login}")
        this.mockMvc.perform(delete("/users/totoDelete"))
                .andExpect(status().isNoContent());
    }

    /**
     * Test the delete user not found method.
     */
    @Test
    public void testDeleteUserNotFound() throws Exception {
        this.mockMvc.perform(delete("/users/totoDeleteNotFound"))
                .andExpect(status().isNotFound());
    }









}
