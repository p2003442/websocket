package fr.univlyon1.m1if.m1if13.users.controller;


import fr.univlyon1.m1if.m1if13.users.dao.UserDao;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserLoginDto;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserLoginPasswordDto;
import fr.univlyon1.m1if.m1if13.users.model.User;
import fr.univlyon1.m1if.m1if13.utils.TodosM1if03JwtHelper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import javax.naming.AuthenticationException;
import java.util.NoSuchElementException;
import java.util.Optional;


@Controller
@Tag(name = "User", description = "The user API")
public class UsersOperationsController {

    // TODO récupérer le DAO...
    /**
     * Le DAO des utilisateurs.
     */
    private UserDao userDao;

    /**
     * Constante pour récupérer apartir du caractère 7 du token.
     */
    private static final int SEPT = 7;

    /**
     * Constructeur de la classe.
     * @param userDao1 Le DAO des utilisateurs.
     */
    @Autowired
    public UsersOperationsController(final UserDao userDao1) {
        this.userDao = userDao1;
    }

    /**
     * Procédure de login utilisée par un utilisateur.
     * @param user Un objet contenant le login et le password de l'utilisateur.
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une ResponseEntity avec le JWT dans le header "Authentication"
     * si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @Operation(summary = "Login",
            description = "Login an user",
            responses = {
                    @ApiResponse(responseCode = "200", description = "User logged in"),
                    @ApiResponse(responseCode = "401", description = "User not logged in"),
                    @ApiResponse(responseCode = "400", description = "Bad request"),
                    @ApiResponse(responseCode = "404", description = "User not found"),
                    @ApiResponse(responseCode = "403", description = "Wrong CORS origin, Forbidden")})
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    @ResponseBody
    public ResponseEntity<Void> loginURLENCODED(
            final @ModelAttribute UserLoginPasswordDto user,
            final @RequestHeader("Origin") String origin)
            throws AuthenticationException {
        return loginMethods(user.getLogin(), user.getPassword(), origin);
    }

    /**
     * Procédure de login utilisée par un utilisateur.
     * @param user
     * @param origin
     * @return Une ResponseEntity avec le JWT dans le header "Authentication"
     * @throws AuthenticationException
     */
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Void> loginJSON(
            final @RequestBody UserLoginPasswordDto user,
            final @RequestHeader("Origin") String origin)
            throws AuthenticationException {
        return loginMethods(user.getLogin(), user.getPassword(), origin);
    }

    /**
     * Méthode de login utilisée par un utilisateur.
     * @param login
     * @param password
     * @param origin
     * @return Une ResponseEntity avec le JWT dans le header "Authentication"
     * @throws AuthenticationException
     */
    private ResponseEntity<Void> loginMethods(
            final String login,
            final String password,
            final String origin) throws AuthenticationException {
        Optional<User> userTent = userDao.get(login);
        if (userTent.isPresent()) {
            User userEx = userTent.get();
            userEx.authenticate(password);
            if (userEx.isConnected()) {
                HttpHeaders headers = new HttpHeaders();
                headers.add("Access-Control-Expose-Headers", "Authorization");
                headers.add("Authorization", "Bearer " + TodosM1if03JwtHelper.generateToken(login, origin));
                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);

            } else {
                throw new AuthenticationException("User not logged in");
            }
        } else {
            throw new NoSuchElementException("User not found");
        }
    }

    /**
     * Réalise la déconnexion.
     * @param login Le login de l'utilisateur.
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @Operation(summary = "Logout",
            description = "Logout an user",
            responses = {
                    @ApiResponse(responseCode = "204", description = "User logged out"),
                    @ApiResponse(responseCode = "400", description = "Bad request"),
                    @ApiResponse(responseCode = "401", description = "User not logged in"),
                    @ApiResponse(responseCode = "404", description = "User not found"),
                    @ApiResponse(responseCode = "403", description = "Wrong CORS origin, Forbidden")})
    @PostMapping(value = "/logout", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    @ResponseBody
    public ResponseEntity<Void> logoutURLENCODED(
            final @ModelAttribute UserLoginDto login,
            final @RequestHeader("Origin") @Parameter(description = "The origin of the request") String origin)
            throws NoSuchElementException {
        return logoutMethods(login, origin);
    }

    /**
     * Réalise la déconnexion.
     * @param login
     * @param origin
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     * @throws NoSuchElementException
     */
    @PostMapping(value = "/logout", consumes = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    @ResponseBody
    public ResponseEntity<Void> logoutJSON(
            final @RequestBody @Parameter(description = "The user login")UserLoginDto login,
            final @RequestHeader("Origin") @Parameter(description = "The origin of the request") String origin)
            throws NoSuchElementException {
        return logoutMethods(login, origin);
    }

    /**
     * Réalise la déconnexion.
     * @param login
     * @param origin
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     * @throws NoSuchElementException
     */
    private ResponseEntity<Void> logoutMethods(
            final UserLoginDto login,
            final String origin) throws NoSuchElementException {
        User user = userDao.get(login.getLogin()).get();
        if (user.isConnected()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + TodosM1if03JwtHelper.generateToken(login.getLogin(), 0, origin));
            user.disconnect();
            return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User not logged in");
        }
    }

    /**
     * Méthode destinée au serveur Node pour valider
     * l'authentification d'un utilisateur.
     * @param jwt Le token JWT qui se trouve dans le header "Authorization" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */

    @Operation(summary = "Authenticate",
            description = "Authenticate an user",
            responses = {
                    @ApiResponse(responseCode = "204", description = "User authenticated"),
                    @ApiResponse(responseCode = "400", description = "Bad request"),
                    @ApiResponse(responseCode = "401", description = "User not authenticated")})
    @GetMapping("/authenticate")
    public ResponseEntity<UserLoginDto> authenticate(
            final @RequestParam("jwt") @Parameter(description = "The JWT token") String jwt,
            final @RequestParam("origin") @Parameter(description = "The origin of the request") String origin)
            throws AuthenticationException {
        String login = jwt;
        if (jwt.startsWith("Bearer ")) {
            login = jwt.substring(SEPT); // Retire les sept premiers caractères
        }
        try {
            String verifiedToken = TodosM1if03JwtHelper.verifyToken(login, origin);
            User user = userDao.get(verifiedToken).get();
            if (user.isConnected()) {
                UserLoginDto userLoginDto = new UserLoginDto(verifiedToken);
                return new ResponseEntity<>(userLoginDto, HttpStatus.OK);
            } else {
                throw new AuthenticationException("User not authenticated");
            }
        } catch (NullPointerException e) {
            throw new NoSuchElementException("User not found");
        } catch (Exception e) {
            throw new AuthenticationException("User not authenticated");
        }
    }
}
