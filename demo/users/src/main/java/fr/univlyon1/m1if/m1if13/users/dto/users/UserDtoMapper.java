package fr.univlyon1.m1if.m1if13.users.dto.users;


import fr.univlyon1.m1if.m1if13.users.dao.Dao;
import fr.univlyon1.m1if.m1if13.users.dao.UserDao;
import fr.univlyon1.m1if.m1if13.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

/**
 * Application du pattern DTO.<br>
 * Réalise le mapping pour les différents types de DTO de <code>User</code>, en entrée et en sortie du serveur.
 *
 * @author Lionel Médini
 */
@Configuration
public class UserDtoMapper {
    /**
     * Le DAO pour les utilisateurs.
     */
    private Dao<User> userDao;


    /**
     * Constructeur de la classe.
     * @param userDao1
     */
    @Autowired
    public UserDtoMapper(final UserDao userDao1) {
        userDao = userDao1;
    }


    /**
     * Renvoie une instance de <code>User</code> à partir d'un objet métier <code>UserRequestDto</code>.
     * Si un objet d'id identique est trouvé dans le DAO, renvoie cet objet, en recopiant dedans les
     * propriétés spécifiées par la requête.
     * Sinon, renvoie une nouvelle instance de l'objet.
     *
     * @param userRequestDto Une instance de <code>UserRequestDto</code> construite à partir d'une requête
     * @return Une instance de <code>User</code> correspondante
     */
    public User toUser(final UserRequestDto userRequestDto) throws ResponseStatusException {
        Optional<User> user;
        user = userDao.get(userRequestDto.getLogin());
        if (user.isPresent()) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "User already exists");
        } else {
            if (userRequestDto.getSpecies() == null
                    || userRequestDto.getPassword() == null || userRequestDto.getLogin() == null) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Missing parameters");
            } else {
                return new User(userRequestDto.getLogin(),
                        userRequestDto.getSpecies(), userRequestDto.getPassword());
            }
        }
    }

    /**
     * Renvoie une instance de <code>User</code> à partir d'un objet métier <code>UserRequestUpdateDto</code>.
     * @param userRequestDto
     * @param login
     * @return
     * @throws Exception
     * @return Une instance de <code>User</code> correspondante
     */
    public User toUser(final UserRequestUpdateDto userRequestDto, final String login) throws ResponseStatusException {
        Optional<User> user;
        user = userDao.get(login);
        if (user.isPresent()) {
            userDao.update(user.get(),
                    new String[]{userRequestDto.getPassword(), String.valueOf(userRequestDto.getSpecies())});
            return userDao.get(login).get();
        } else {
            if (userRequestDto.getSpecies() == null
                    || userRequestDto.getSpecies().toString().isEmpty()
                    || userRequestDto.getPassword() == null
                    || userRequestDto.getPassword().isEmpty()) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Missing parameters");
            } else {
                User userCreated = new User(login,
                        userRequestDto.getSpecies(), userRequestDto.getPassword());
                userDao.save(userCreated);
                return userCreated;
            }
        }
    }
}
