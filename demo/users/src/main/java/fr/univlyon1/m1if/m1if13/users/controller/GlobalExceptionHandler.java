package fr.univlyon1.m1if.m1if13.users.controller;

import fr.univlyon1.m1if.m1if13.users.Configuration.ErrorInfo;
import jakarta.servlet.http.HttpServletRequest;
import javax.naming.AuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.util.NoSuchElementException;


/**
 * This class is the global exception handler.
 */
@ControllerAdvice
public class GlobalExceptionHandler {


    /*
    class teste a effacer si tout marche bien
    @ExceptionHandler(value = Exception.class)
    public ModelAndView
    defaultErrorHandler(final HttpServletRequest req, final Exception e) throws Exception {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it - like the OrderNotFoundException example
        // at the start of this post.
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
            throw e;
            }

        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }*/

    /**
     * This method handles the no such element exception.
     * @param req
     * @param ex
     * @return the model and view
     */
    @ExceptionHandler(NoSuchElementException.class)
    public ModelAndView handleNoSuchElementException(final HttpServletRequest req, final Exception ex) {
        return handleBadRequest(req, ex, HttpStatus.NOT_FOUND);
    }

    /**
     * This method handles the authentication exception.
     * @param req
     * @param ex
     * @return the model and view
     */
    @ExceptionHandler(AuthenticationException.class)
    public ModelAndView handleAuthenticationException(final HttpServletRequest req, final Exception ex) {
        return handleBadRequest(req, ex, HttpStatus.UNAUTHORIZED);
    }
    /**
     * This method handles the bad request.
     * @param req
     * @param ex
     * @return the model and view
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView handleBadRequest(final HttpServletRequest req, final Exception ex) {
        return handleBadRequest(req, ex, HttpStatus.BAD_REQUEST);
    }

    /**
     * This method handles the response status exception.
     * @param req
     * @param ex
     * @return the model and view
     */
    @ExceptionHandler(ResponseStatusException.class)
    public ModelAndView handleResponseStatusException(final HttpServletRequest req, final ResponseStatusException ex) {
        ErrorInfo errorInfo = new ErrorInfo(req.getRequestURL().toString(), ex);
        return errorInfo.redirectError((HttpStatus) ex.getStatusCode());
    }

    /**
     * This method handles the bad request.
     * @param req
     * @param ex
     * @param ex1
     * @return the model and view
     */
    public ModelAndView handleBadRequest(final HttpServletRequest req, final Exception ex, final HttpStatus ex1) {
        ErrorInfo errorInfo = new ErrorInfo(req.getRequestURL().toString(), ex);
        return errorInfo.redirectError(ex1);
    }
//    @ExceptionHandler(ResponseStatusException.class)
//    public ResponseEntity<Void> handleResponseStatusException(final ResponseStatusException ex) {
//        return ResponseEntity.status(ex.getStatusCode()).build();
//    }


}
