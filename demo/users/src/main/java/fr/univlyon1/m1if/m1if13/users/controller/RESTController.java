package fr.univlyon1.m1if.m1if13.users.controller;

import fr.univlyon1.m1if.m1if13.users.dao.UserDao;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserRequestDto;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserDtoMapper;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserRequestUpdateDto;
import fr.univlyon1.m1if.m1if13.users.dto.users.UserResponsesanspswDto;
import fr.univlyon1.m1if.m1if13.users.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.stereotype.Controller;

import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;


import java.util.Collection;
import java.util.Optional;

/**
 * This class is the REST controller.
 */
@Controller
@ResponseBody
@Tag(name = "User", description = "The user API")
public class RESTController {

    /**
     * The user dao.
     */
    private UserDao userDao;

    /**
     * The user dto mapper.
     */
    private UserDtoMapper mapper;

    /**
     * The constructor of the class.
     * @param userDao1 the user dao to set
     */
    @Autowired
    public RESTController(final UserDao userDao1) {
        this.userDao = userDao1;
        this.mapper = new UserDtoMapper(userDao1);
    }


    /**
     * This method returns the logins of the users.
     * @return la liste des logins des utilisateurs
     */
    @Operation(summary = "Get all users",
            responses = {
                    @ApiResponse(description = "List of users",
                            content = {
                                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
                                    @Content(mediaType = MediaType.APPLICATION_XML_VALUE),
                                    @Content(mediaType = MediaType.TEXT_HTML_VALUE)
                            },
                            responseCode = "200")})
    @GetMapping(value = "/users", produces = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE})
    public Collection<String> getUserDao() {
        return userDao.getAll();
    }

    /**
     * This method returns the logins of the users in HTML.
     * @return the model and view
     */
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    @GetMapping(value = "/users", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView getAllUsersHTML() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("users", userDao.getAll());
        mav.setViewName("users");
        return mav;
    }

    /**
     * This method returns the user with the given login.
     * @param login the login of the user
     * @return the user
     */
    @Operation(summary = "Get a user",
            responses = {
                    @ApiResponse(description = "User",
                            content = {
                                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                                            schema = @Schema(implementation = UserRequestDto.class)),
                                    @Content(mediaType = MediaType.APPLICATION_XML_VALUE,
                                            schema = @Schema(implementation = UserRequestDto.class)),
                                    @Content(mediaType = MediaType.TEXT_HTML_VALUE)
                            },
                            responseCode = "200"),
                    @ApiResponse(responseCode = "404", description = "User not found", content = @Content())})
    @GetMapping(value = "/users/{login}", produces = {
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE})
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.50",
            "https://192.168.75.50", "http://localhost:3376"})
    public ResponseEntity<UserResponsesanspswDto>  getUser(@PathVariable("login")
                            @Parameter(description = "The login of the user", required = true) final String login) {
        Optional<User> user = userDao.get(login);
        if (user.isPresent()) {
            UserResponsesanspswDto userRequest =
                    new UserResponsesanspswDto(user.get().getLogin(), user.get().getSpecies());
            return new ResponseEntity<>(userRequest, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * This method returns the user with the given login in HTML.
     * @param login the login of the user
     * @return the model and view
     */
    @GetMapping(value = "/users/{login}", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView getUserHTML(@PathVariable("login") final String login) {
        ModelAndView mav = new ModelAndView();
        Optional<User> user = userDao.get(login);
        if (user.isPresent()) {
            UserResponsesanspswDto userRequest =
                    new UserResponsesanspswDto(user.get().getLogin(), user.get().getSpecies());
            mav.addObject("user", userRequest);
            mav.setViewName("user");
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return mav;
    }

    /**
     * This method add a user.
     * @param newUser the user to add
     * @return the response entity
     */
    @Operation(summary = "Add a user",
            responses = {
                    @ApiResponse(responseCode = "201", description = "User added", content = @Content()),
                    @ApiResponse(responseCode = "409", description = "User already exists", content = @Content()),
                    @ApiResponse(responseCode = "400", description = "Bad Argument", content = @Content())})
    @PostMapping(value = "/users", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseEntity<Void> createFormEncoded(
            @ModelAttribute final UserRequestDto newUser) throws ResponseStatusException {
        userDao.save(mapper.toUser(newUser));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", "users/" + newUser.getLogin());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * This method add a user.
     * @param newUser the user to add
     * @throws Exception if the user already exists
     * @return the response entity
     */
    @PostMapping(value = "/users", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<Void> create(@RequestBody final UserRequestDto newUser) throws ResponseStatusException {
        userDao.save(mapper.toUser(newUser));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", "users/" + newUser.getLogin());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * This method update the user password and or species.
     * @param login the login of the user
     * @param userRequest the user request to update
     * @return the response entity
     */
    @Operation(summary = "Update user",
            description = "Update an user",
            responses = {
                    @ApiResponse(responseCode = "204", description = "User updated", content = @Content()),
                    @ApiResponse(responseCode = "201", description = "User created", content = @Content()),
                    @ApiResponse(responseCode = "400", description = "Bad request", content = @Content())
            }
    )
    @PutMapping(value = "/users/{login}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public ResponseEntity<Void> updateUserENCODED(@PathVariable("login") final String login,
            @ModelAttribute final UserRequestUpdateDto userRequest) throws ResponseStatusException {
        return putMethods(login, userRequest);
    }

    /**
     * This method update the user password and or species.
     * @param login the login of the user
     * @param userRequest the user request to update
     * @return the response entity
     */
    @PutMapping(value = "/users/{login}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateUserJSON(@PathVariable("login") final String login,
            @RequestBody final UserRequestUpdateDto userRequest) throws ResponseStatusException {
        return putMethods(login, userRequest);
    }

    /**
     * This method update the user password and or species.
     * @param login the login of the user
     * @param userRequest the user request to update
     * @return the response entity
     */
    private ResponseEntity<Void> putMethods(
            final @PathVariable("login") String login,
            final @ModelAttribute UserRequestUpdateDto userRequest) {
        Optional<User> userToModif = userDao.get(login);
        if (userToModif.isPresent()) {
            mapper.toUser(userRequest, login);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Location", "users/" + login);
            return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
        } else {
            mapper.toUser(userRequest, login);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Location", "users/" + login);
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
    }

    /**
     * This method delete a user.
     * @param login the login of the user
     * @return the response entity
     */
    @Operation(summary = "Delete user",
            description = "Delete an user",
            responses = {
                    @ApiResponse(responseCode = "204", description = "User deleted", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "User not found", content = @Content())})
    @DeleteMapping(value = "/users/{login}")
    public ResponseEntity<Void> deleteUser(
            @PathVariable("login") @Parameter(description = "The user login") final String login) {
        Optional<User> userToModif = userDao.get(login);
        if (userToModif.isPresent()) {
            userDao.delete(userToModif.get());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
    }


}
