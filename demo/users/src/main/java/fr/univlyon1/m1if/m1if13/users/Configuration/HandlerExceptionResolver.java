package fr.univlyon1.m1if.m1if13.users.Configuration;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is the handler exception resolver.
 */
public interface HandlerExceptionResolver {
    /**
     * This method resolves the exception.
     * @param request the request
     * @param response the response
     * @param handler the handler
     * @param ex the exception
     * @return the model and view
     */
    ModelAndView resolveException(HttpServletRequest request,
                                  HttpServletResponse response, Object handler, Exception ex);
}
