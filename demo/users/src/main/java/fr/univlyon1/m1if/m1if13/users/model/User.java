package fr.univlyon1.m1if.m1if13.users.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.naming.AuthenticationException;


public class User {
    /**
     * The login of the user.
     */
    private final String login;
    /**
     * The species of the user.
     */
    private Species species;
    /**
     * The password of the user.
     */
    private String password;
    // Permet d'invalider une connexion même si le token est toujours valide
    /**
     * The connection status of the user.
     */
    private boolean connected = false;
    // Nom du fichier image qui représentera l'utilisateur sur la carte
    /**
     * The image of the user.
     */
    private String image;

    /**
     * Constructor of the user.
     * @param newLogin
     * @param newSpecies
     * @param newPassword
     */
    @JsonCreator
    public User(@JsonProperty("login") final String newLogin,
                @JsonProperty("species") final Species newSpecies,
                @JsonProperty("password") final String newPassword) {
        this.login = newLogin;
        this.species = newSpecies;
        this.password = newPassword;
    }

    /**
     * Constructor of the user.
     * @param newLogin
     * @param newSpecies
     * @param newPassword
     * @param newImage
     */
    public User(final String newLogin, final Species newSpecies, final String newPassword, final String newImage) {
        this.login = newLogin;
        this.species = newSpecies;
        this.password = newPassword;
        this.image = newImage;
    }

    /**
     * Getter of the login.
     * @return login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Getter of the species.
     * @return species
     */
    public Species getSpecies() {
        return species;
    }

    /**
     * Setter of the species.
     * @param newSpecies
     */
    public void setSpecies(final Species newSpecies) {
        this.species = newSpecies;
    }


    /**
     * Getter of the password.
     * @return password
     */
    public String getPassword() {
        return password;
    }
    /**
     * Setter of the password.
     * @param newPassword
     */
    public void setPassword(final String newPassword) {
        this.password = newPassword;
    }

    /**
     * Getter of the connection status.
     * @return connected
     */
    public boolean isConnected() {
        return this.connected;
    }

    /**
     * This method authenticates the user.
     * @param newPassword
     * @throws AuthenticationException
     */
    public void authenticate(final String newPassword) throws AuthenticationException {
        if (!newPassword.equals(this.password)) {
            throw new AuthenticationException("Mot de passe incorrect");
        }
        this.connected = true;
    }

    /**
     * This method disconnects the user.
     */
    public void disconnect() {
        this.connected = false;
    }

    /**
     * Getter of the image.
     * @return image
     */
    public String getImage() {
        return image;
    }
    /**
     * Setter of the image.
     * @param newImage
     */
    public void setImage(final String newImage) {
        this.image = newImage;
    }

}
