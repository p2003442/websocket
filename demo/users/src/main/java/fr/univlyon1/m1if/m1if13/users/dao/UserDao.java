package fr.univlyon1.m1if.m1if13.users.dao;

import fr.univlyon1.m1if.m1if13.users.model.Species;
import fr.univlyon1.m1if.m1if13.users.model.User;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Component
public class UserDao implements Dao<User> {
    /**
     * The set of users.
     */
    private Set<User> users = new HashSet<>();

    /**
     * The constructor of the class.
     */

    public UserDao() {
        users.add(new User("admin", Species.ADMIN, "admin"));
    }

    /**
     * This method returns the user with the given login.
     * @param id Login de l'utilisateur
     * @return
     */
    @Override
    public Optional<User> get(final String id) {
        for (User user : users) {
            if (user.getLogin().equals(id)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    /**
     * This method returns all the logins of the users.
     * @return
     */
    @Override
    public Set<String> getAll() {
        Set<String> logins = new HashSet<>();
        for (User user : users) {
            logins.add(user.getLogin());
        }
        return logins;
    }

    /**
     *  This method creates a user and saves it.
     * @param user L'utilisateur à créer
     */
    @Override
    public void save(final User user) {
        users.add(user);
    }

    /**
     * This method updates the user with the given login.
     * @param user L'utilisateur à modifier
     * @param params Un tableau de 2 string : password/ Species
     */
    @Override
    public void update(final User user, final String[] params) throws ResponseStatusException {
        if (!Objects.equals(params[0], "") && !Objects.equals(params[1], "null")) {
            user.setPassword(params[0]);
            user.setSpecies(Species.valueOf(params[1]));
        } else {
            if (!Objects.equals(params[0], "")) {
                user.setPassword(params[0]);
            } else if (!Objects.equals(params[1], "null")) {
                user.setSpecies(Species.valueOf(params[1]));
            } else {
                throw new ResponseStatusException(
                        org.springframework.http.HttpStatus.BAD_REQUEST, "Bad or Missing parameters");
            }
        }
    }

    /**
     * This method deletes the user with the given login.
     * @param user L'utilisateur à supprimer
     */
    @Override
    public void delete(final User user) {
        users.remove(user);
    }
}
