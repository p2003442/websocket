package fr.univlyon1.m1if.m1if13.users.dto.users;


/**
 * DTO contenant les données que peut recevoir le serveur pour créer ou mettre à jour un utilisateur.
 *
 * @author Lionel Médini
 */
public class UserLoginDto {
    /**
     * Le login de l'utilisateur.
     */
    private String login;


    //<editor-fold desc="Constructeurs">
    /**
     * Crée un <code>UserRequestDto</code> vide.<br>
     * Appelé par Jackson dans <code>ContentNegotiationHelper</code>, cf. :
     * <code><a href="http://fasterxml.github.io/jackson-core/javadoc/2.13/com/fasterxml/
     * jackson/core/ObjectCodec.html">ObjectCodec</a>.readValue()</code>.
     */
    public UserLoginDto() {
        super();
    }

    /**
     * Crée un <code>UserRequestDto</code> à l'aide des paramètres de la requête.<br>
     * Appelé "à la main" par <code>ContentNegotiationHelper.applicationSpecificProcessing()</code>.
     * @param login1 Le login de l'utilisateur (peut se trouver dans un paramètre ou dans l'URL)
     */

    public UserLoginDto(final String login1) {
        this.login = login1;
    }
    //</editor-fold>

    //<editor-fold desc="Setters">
    /**
     * Positionne le login de l'utilisateur (pour utilisation par un <code>CodecMapper</code> Jackson).
     * @param login1 Le login de l'utilisateur présent dans la requête
     */
    public void setLogin(final String login1) {
        this.login = login1;
    }



    //<editor-fold desc="Getters">
    /**
     * Renvoie le login de l'utilisateur passé dans la requête.
     * @return Le login de l'utilisateur passé dans la requête
     */
    public String getLogin() {
        return login;
    }

    //</editor-fold>
}
