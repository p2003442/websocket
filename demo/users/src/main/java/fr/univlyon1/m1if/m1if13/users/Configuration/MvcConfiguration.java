package fr.univlyon1.m1if.m1if13.users.Configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



/**
 * This class is the MVC configuration.
 */
@Configuration
public class MvcConfiguration implements WebMvcConfigurer {


    /**
     * This method returns the exception resolver.
     * @return the exception resolver
     */
    @Bean
    public MyMappingExceptionResolver myMappingExceptionResolver() {
        return new MyMappingExceptionResolver();
    }


    /**
     * Confuguration of the content negotiation.
     * @param configurer the configurer
     */
    @Override
    public void configureContentNegotiation(final ContentNegotiationConfigurer configurer) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }
}
