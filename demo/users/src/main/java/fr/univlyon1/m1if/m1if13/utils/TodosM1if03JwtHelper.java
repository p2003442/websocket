package fr.univlyon1.m1if.m1if13.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;



import jakarta.validation.constraints.NotNull;

import java.util.Date;

/**
 * Classe qui centralise les opérations de validation et de génération d'un token "métier", c'est-à-dire dédié
 * à cette application.
 *
 * @author Lionel Médini
 */
public final class TodosM1if03JwtHelper {
    /**
     * La clé secrète pour signer les tokens.
     */
    private static final String SECRET = "monsecret2024";
    /**
     * L'émetteur du token.
     */
    private static final String ISSUER = "MIF-USER 2024";
    /**
     * La durée de vie d'un token.
     */
    private static final long LIFETIME = 1800000; // Durée de vie d'un token : 30 minutes ; vous pouvez le modifier
    // pour tester
    /**
     * L'algorithme de signature des tokens.
     */
    private static final Algorithm ALGORITHM = Algorithm.HMAC256(SECRET);

    /**
     * Pour valider la règle <code>HideUtilityClassConstructorCheck</code> de CheckStyle...
     * Une classe utilitaire qui n'a que des méthodes statiques ne doit pas avoir de constructeur
     * public, parce qu'il ne servirait à rien de l'instancier.
     */
    private TodosM1if03JwtHelper() {
        throw new UnsupportedOperationException();
    }

    /**
     * Vérifie l'authentification d'un utilisateur grâce à un token JWT.
     *
     * @param token le token à vérifier
     * @param origin  l'origine de la requête (pour la comparer avec celle du client, stockée dans
     *                le token JWT)
     * @return un booléen qui indique si le token est bien formé et valide (pas expiré) et si
     * l'utilisateur est authentifié
     */
    public static String verifyToken(final String token, final @NotNull String origin) throws NullPointerException,
            JWTVerificationException {
        JWTVerifier authenticationVerifier = JWT.require(ALGORITHM)
                .withIssuer(ISSUER)
                .withAudience(origin) // Non-reusable verifier instance
                .build();

        authenticationVerifier.verify(token); // Lève une NullPointerException si le token n'existe pas, et une
        // JWTVerificationException s'il est invalide
        DecodedJWT jwt = JWT.decode(token); // Pourrait lever une JWTDecodeException mais comme le token est
        // vérifié avant, cela ne devrait pas arriver
        return jwt.getClaim("sub").asString();
    }




    /**
     * Crée un token avec les caractéristiques de l'utilisateur.
     *
     * @param subject le login de l'utilisateur
     * @param origin    l'origine de la requête (pour la comparer avec celle du client,
     *                  stockée dans le token JWT)
     * @return le token signé
     * @throws JWTCreationException si les paramètres ne permettent pas de créer un token
     */
    public static String generateToken(final String subject, final String origin)
            throws JWTCreationException {
        return JWT.create()
                .withIssuer(ISSUER)
                .withSubject(subject)
                .withAudience(origin)
                .withExpiresAt(new Date(new Date().getTime() + LIFETIME))
                .sign(ALGORITHM);
    }

    /**
     * Crée un token avec les caractéristiques de l'utilisateur.
     * @param subject le login de l'utilisateur
     * @param life la durée de vie du token
     * @param origin l'origine de la requête (pour la comparer avec celle du client,
     *               stockée dans le token JWT)
     * @return le token signé
     * @throws JWTCreationException si les paramètres ne permettent pas de créer un token
     */
    public static String generateToken(final String subject, final long life, final String origin)
            throws JWTCreationException {
        return JWT.create()
                .withIssuer(ISSUER)
                .withSubject(subject)
                .withAudience(origin)
                .withExpiresAt(new Date(new Date().getTime() + life))
                .sign(ALGORITHM);
    }


}
