package fr.univlyon1.m1if.m1if13.users.dto.users;

import fr.univlyon1.m1if.m1if13.users.model.Species;

/**
 * This class is the user request update dto.
 */
public class UserRequestUpdateDto {

    /**
     * The password of the user.
     */
    private String password;

    /**
     * The species of the user.
     */
    private Species species;

    /**
     * Constructor of the user request update dto.
     */
    public UserRequestUpdateDto() {
        super();
    }

    /**
     * Constructor of the user request update dto.
     * @param newPassword
     * @param newSpecies
     */
    public UserRequestUpdateDto(final String newPassword, final Species newSpecies) {
        this.password = newPassword;
        this.species = newSpecies;
    }

    /**
     * Getter of the password.
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter of the password.
     * @param newPassword
     */
    public void setPassword(final String newPassword) {
        this.password = newPassword;
    }

    /**
     * Getter of the species.
     * @return species
     */
    public Species getSpecies() {
        return species;
    }

    /**
     * Setter of the species.
     * @param newSpecies
     */
    public void setSpecies(final Species newSpecies) {
        this.species = newSpecies;
    }

}
