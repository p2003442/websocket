package fr.univlyon1.m1if.m1if13.users.Configuration;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class MyMappingExceptionResolver extends SimpleMappingExceptionResolver {
    /**
     * This method is the constructor of the exception resolver.
     */
    public MyMappingExceptionResolver() {
        // Enable logging by providing the name of the logger to use
        setWarnLogCategory(MyMappingExceptionResolver.class.getName());
    }

    /**
     * This method builds the log message.
     * @param e the exception
     * @param req the request
     * @return the log message
     */
    @Override
    public String buildLogMessage(final Exception e, final HttpServletRequest req) {
        return "MVC exception: " + e.getLocalizedMessage();
    }

    /**
     * This method resolves the exception.
     * @param req the request
     * @param resp the response
     * @param handler the handler
     * @param ex the exception
     * @return the model and view
     */
    @Override
    protected ModelAndView doResolveException(final HttpServletRequest req, final HttpServletResponse resp,
                                              final Object handler, final Exception ex) {
        // Call super method to get the ModelAndView
        ModelAndView mav = super.doResolveException(req, resp, handler, ex);

        // Make the full URL available to the view - note ModelAndView uses
        // addObject() but Model uses addAttribute(). They work the same.
        assert mav != null;
        mav.addObject("url", req.getRequestURL());

        // Use your ErrorInfo class to hold the error information
        ErrorInfo errorInfo = new ErrorInfo(req.getRequestURL().toString(), ex);
        mav.addObject("errorInfo", errorInfo);

        return mav;
    }
}
