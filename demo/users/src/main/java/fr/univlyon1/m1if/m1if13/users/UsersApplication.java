package fr.univlyon1.m1if.m1if13.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersApplication {
    /**
    * Main method.
    * @param args
    */
    public static void main(final String[] args) {
        SpringApplication.run(UsersApplication.class, args);
    }

}
