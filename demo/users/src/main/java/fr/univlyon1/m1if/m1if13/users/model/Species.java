package fr.univlyon1.m1if.m1if13.users.model;

public enum Species {
    /**
     * Pirate species.
     */
    PIRATE,
    /**
     * Villageois species.
     */
    VILLAGEOIS,
    /**
     * Admin species.
     */
    ADMIN
}
