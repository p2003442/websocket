package fr.univlyon1.m1if.m1if13.users.dto.users;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * DTO contenant les données que peut recevoir le serveur pour créer ou mettre à jour un utilisateur.
 *
 * @author Lionel Médini
 */
public class UserLoginPasswordDto {
    /**
     * Le login de l'utilisateur.
     */
    private String login;
    /**
     * Le password de l'utilisateur.
     */
    private String password;


    //<editor-fold desc="Constructeurs">
    /**
     * Crée un <code>UserRequestDto</code> vide.<br>
     * Appelé par Jackson dans <code>ContentNegotiationHelper</code>, cf. :
     * <code><a href="http://fasterxml.github.io/jackson-core/javadoc/2.13/com/fasterxml/
     * jackson/core/ObjectCodec.html">ObjectCodec</a>.readValue()</code>.
     */
    public UserLoginPasswordDto() {
        super();
    }

    /**
     * Crée un <code>UserRequestDto</code> à l'aide des paramètres de la requête.<br>
     * Appelé "à la main" par <code>ContentNegotiationHelper.applicationSpecificProcessing()</code>.
     * @param login1 Le login de l'utilisateur (peut se trouver dans un paramètre ou dans l'URL)
     * @param password1 Le password (dans un paramètre)
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public UserLoginPasswordDto(final String login1, final String password1) {
        this.login = login1;
        this.password = password1;
    }
    //</editor-fold>

    //<editor-fold desc="Setters">
    /**
     * Positionne le login de l'utilisateur (pour utilisation par un <code>CodecMapper</code> Jackson).
     * @param login1 Le login de l'utilisateur présent dans la requête
     */
    public void setLogin(final String login1) {
        this.login = login1;
    }

    /**
     * Positionne le password de l'utilisateur (pour utilisation par un <code>CodecMapper</code> Jackson).
     * @param password1 Le password de l'utilisateur présent dans la requête
     */
    public void setPassword(final String password1) {
        this.password = password1;
    }


    //<editor-fold desc="Getters">
    /**
     * Renvoie le login de l'utilisateur passé dans la requête.
     * @return Le login de l'utilisateur passé dans la requête
     */
    public String getLogin() {
        return login;
    }

    /**
     * Renvoie le password de l'utilisateur passé dans la requête.
     * @return Le password de l'utilisateur passé dans la requête
     */
    public String getPassword() {
        return password;
    }
    //</editor-fold>
}
