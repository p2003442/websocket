package fr.univlyon1.m1if.m1if13.users.Configuration;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is the error information.
 */
public class ErrorInfo {
    /**
     * The URL.
     */
    private final String url;

    /**
     * The exception.
     */
    private final String ex;

    /**
     * this method is the accessor of the URL.
     * @return the URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * this method is the accessor of the exception.
     * @return the exception
     */
    public String getEx() {
        return ex;
    }

    /**
     * This method is the constructor of the error information.
     * @param urln the URL
     * @param exn the exception
     */
    public ErrorInfo(final String urln, final Exception exn) {
        this.url = urln;
        this.ex = exn.getLocalizedMessage();
    }


    /**
     * redirect the error to the error page.
     * @param status the status
     * @return the error page
     */
    public ModelAndView redirectError(final HttpStatus status) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("ex", ex);
        mav.addObject("url", url);
        mav.setStatus(status);
        mav.setViewName("error");
        return mav;
    }
}
