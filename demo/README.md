# Etudiants & ID groupe

- **ID du groupe** : gr04
- **Etudiant** :
    - FAURE Alexandre p2006157
    - KOMLENOVIC Emilien p2000315

# TP1 & TP2

## Liens vers les fichiers

- [Documentation OpenAPI (users-api.yaml)](./users/users-api.yaml)
- [Collection de requêtes Postman](./m1if13-scenario.postman_collection.json)

## Swagger

Vous pouvez accéder à la documentation Swagger générée par Spring et déployée sur notre VM en suivant ce lien : [Swagger Documentation](https://192.168.75.50:8443/users/swagger-ui/index.html)


# TP 4

## Pour le run en local : 

    npm install && cd admin && npm install && cd .. && npm run build && npm start

## Commande pour build
- build dev:  
    npm run build
- build prod:  
    npm run buildprod

## Commande pour lancer le serveur
    
    npm start

## Commande pour les test

    npm test

## Liens important

- lien en local:  
    http://localhost:3376/static/pageOne.html  
- lien sur la vm:  
    https://192.168.75.50/secret/  

