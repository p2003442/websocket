# Websocket

## Websocket c'est quoi ?

- API Websocket
- Communication bidirectionnelle
- Client et serveur

---

## Websocket, pourquoi ?

- Communication en temps réel
- Réduction de la latence
- Peut gérer de nombreux clients, idéal pour système IoT* ( nécessite moins de ressources que HTTP )
    - la taille du message minimum du protocole WebSocket est de 2 octets (0x00 pour le début du message et 0xFF pour la fin). Avec HTTP, il faut compter au moins 200 octets pour un header.  

*Internet des objets, communication entre un serveur et un objet connecté, tq une ampoule connectée, ...

---

## Dans quel cas utiliser Websocket ?

- Messagerie instantanée
- Jeux en ligne
- Flux de données en temps réel (exemple : évènements sportifs)
- Expérience personnelle

---

## Côté Client ?

Tous les navigateurs supportent la technologie Websocket.

### Comment cela fonctionne ?
Voici une liste non-exhaustives des commandes principales.

- Ouvrir un canal de communication (socket)
    ```js
    var exampleSocket = new WebSocket("ws://www.example.com/socketserver");
    ```
- Envoyer des messages
    ```js
    exampleSocket.send("Voici le message envoyé !!");
    ```
- Recevoir des messages (événement)
    ```js
    exampleSocket.addEventListener("message", function (event) {
        console.log("Message reçu du serveur ", event.data);
    });
    ```
- Fermer le canal de communication
    ```js
    exampleSocket.close();
    ```
---

## Côté Serveur ?
- Tous les langages de programmation supporte la technologie Websocket 

### Comment cela fonctionne ?
Voici une liste non exhaustives des commandes principales.
- Ouverture d'une Websocket
```js
const ws = new WebSocketServer({ port: 3377 });
```
- À la connexion d'un client
```js
ws.on('connection', function connection(ws))
```
- Envoie un message a tous ses clients
```js
ws.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
        client.send(data);
    }
});
```

---

- Réception de messages
```js
ws.on('message', function message(data))
```
- Détection de la fermeture de la connexion (volontaire ou non) d'un client. Envoie d'un PING pour vérifier si le client est toujours connecté.

---

## Sécurité Websocket

- Un protocole de sécurité à été crée (semblable à HTTPS)
    - `wss://` (chiffré)
  

---

## Conclusion

Websocket permet de créer du web "en temps réel", et une grande liberté concernant les messages envoyés et reçus. Cependant, cette liberté doit être bien gérée côté serveur. Il est aussi plus difficile d'authentifier un client, qu'avec une API REST par exemple.
