# Slide 1 

L'API WebSocket est une technologie évoluée qui permet d'ouvrir un canal de communication bidirectionnelle entre un client et un serveur. Avec cette API vous pouvez envoyer des messages à un serveur et recevoir ses réponses de manière événementielle sans avoir à aller consulter le serveur pour obtenir une réponse.

---

https://www.ionos.fr/digitalguide/sites-internet/developpement-web/quest-ce-que-le-websocket/